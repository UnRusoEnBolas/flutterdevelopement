import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../classes/alert.dart';
import '../classes/alertmanager.dart';
import '../classes/user.dart';

class AlertInputPage extends StatefulWidget {
  @override
  _AlertInputPageState createState() => _AlertInputPageState();

  User currentUser;

  AlertInputPage(this.currentUser);
}

class _AlertInputPageState extends State<AlertInputPage> {
  
  TextEditingController _titleTextController = TextEditingController();
  TextEditingController _bodyTextController = TextEditingController();
  String _categoryValueController = "BRIQUETAS";

  Widget _myTitleTextField(TextEditingController _controller){
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: TextField(
        maxLines: 1,
        controller: _controller,
        style: TextStyle(fontSize: 21, fontWeight: FontWeight.bold),
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          hintText: "Introduce un título",
        ),
      ),
    );
  }

  Widget _myBodyTextField(TextEditingController _controller){
    return TextField(
        maxLines: 5,
        controller: _controller,
        style: TextStyle(fontSize: 15), 
        textAlign: TextAlign.justify,
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          hintText: "Especifica que quieres que notifique la alerta...",
        ),
      );
  }

  Widget _myDropDownButton(Function setState){
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Center(
        child: Theme(
          data: Theme.of(context).copyWith(
            canvasColor: Colors.blue
          ),
                child: DropdownButton<String>(
            value: _categoryValueController,
            icon: Icon(Icons.arrow_drop_down, color: Colors.white,),
            hint: Text("Categoria"),
            items: [
              DropdownMenuItem<String>(value: "BRIQUETAS", child: Text("Briquetas", style: TextStyle(color: Colors.white),),),
              DropdownMenuItem<String>(value: "CONSUMO", child: Text("Consumo", style: TextStyle(color: Colors.white),)),
              DropdownMenuItem<String>(value: "AVERIAS", child: Text("Averias", style: TextStyle(color: Colors.white),)),
              DropdownMenuItem<String>(value: "MAT. PRIMA", child: Text("Materia prima", style: TextStyle(color: Colors.white),)),
              DropdownMenuItem<String>(value: "ESCORIA", child: Text("Escoria", style: TextStyle(color: Colors.white),)),
            ],
            onChanged: (val){
              setState(() {
               _categoryValueController=val; 
              });
            },
          ),
        ),
      ),
    );
  }

  Widget _buildBody(var alertManager, Function setState){
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: ListView(
          children: <Widget>[
            _myDropDownButton(setState),
            _myTitleTextField(_titleTextController),
            _myBodyTextField(_bodyTextController)
          ],
        ),
      ),
    );
  }

  Widget _buildFAB(var alertManager, BuildContext context){
    return FloatingActionButton.extended(
      icon: Icon(Icons.send, color: Colors.blue),
      label: Text("Enviar alerta", style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),),
      backgroundColor: Colors.white,
      onPressed: (){
        if(_titleTextController.text.isEmpty || _bodyTextController.text.isEmpty){
          showDialog(
            barrierDismissible: false,
            context: context,
            builder: (context){
              return AlertDialog(
                elevation: 24,
                title: Text("Alerta", style: TextStyle(fontWeight: FontWeight.bold),),
                content: Text("Se deben rellenar todos los campos para poder enviar la alerta."),
                actions: <Widget>[
                  FlatButton(
                    child: Text("Ok"),
                    onPressed: ()=>Navigator.of(context).pop(),
                  )
                ],
              );
            }
          );
        } else {
          DateTime rightNow = DateTime.now();
          alertManager.addNew(
            Alert(
              body: _bodyTextController.text,
              title: _titleTextController.text,
              category: _categoryValueController,
              hasBeenChecked: false,
              publishedTime: "${rightNow.toIso8601String().substring(11, 19)}",
              publishedDate: "${rightNow.year}-${rightNow.month.toString().padLeft(2,'0')}-${rightNow.day.toString().padLeft(2,'0')}",
            )
          );
          Navigator.of(context).pop();
        }
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    var alertManager = Provider.of<AlertManager>(context);
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        title: Text("Añadir alerta"),
      ),
      body: _buildBody(alertManager, setState),
      floatingActionButton: _buildFAB(alertManager, context),
    );
  }
}