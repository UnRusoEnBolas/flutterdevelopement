import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../classes/user.dart';
import '../classes/datasheet.dart';
import '../classes/datasheetmanager.dart';
import '../classes/cubasbuttonmanager.dart';
import '../classes/cajasbuttonmanager.dart';
import '../classes/lingotesbuttonmanager.dart';

GlobalKey _scaffoldKey;

List<String> listBriquetas = [
  'TB01',
  'TB02',
  'TB03',
  'TB04',
  'TB05',
  'TB06',
  'TB07',
  'TB08',
  'TB09',
  'TB10',
  'TB11',
  'TB12',
  'TB13',
];

class NewDataInputPage extends StatefulWidget {
  @override
  _NewDataInputPageState createState() => _NewDataInputPageState();

  User currentUser;
  DataSheet editSheet;
  int idx;

  NewDataInputPage(this.currentUser, {DataSheet editSheet, int idx}){
    this.editSheet=editSheet;
    this.idx=idx;
  }
}

class _NewDataInputPageState extends State<NewDataInputPage> {
  List<TextEditingController> controllersList = List();
  TextEditingController controllerGasH11;
  TextEditingController controllerGasH9;
  TextEditingController controllerBriqueta1KG;
  TextEditingController controllerBriqueta2KG;
  TextEditingController controllerEscoria;
  TextEditingController controllerNataCuba;
  TextEditingController controllerTiempoLimpieza;
  TextEditingController controllerTiempoParo;
  TextEditingController controllerCubas;
  TextEditingController controllerLingotes;
  TextEditingController controllerCajas;
  String controllerBriqueta1Dropdown;
  String controllerBriqueta2Dropdown;
  bool controllerFinalizado = false;
  int controllerTurno;
  DateTime selectedDate;


  @override
  void initState() {
    if(widget.editSheet!=null){
      DataSheet ds = widget.editSheet;
      Map data = ds.data;
      
      selectedDate = DateTime.parse(ds.uplaodDate);
       controllerTurno = int.parse(data['turno']);
       controllerGasH11 = TextEditingController(text: data['gash11']);
       controllerGasH9 = TextEditingController( text: data['gash9']);
       controllerBriqueta1KG = TextEditingController( text: data['briqueta1kg']);
       controllerBriqueta2KG = TextEditingController( text: data['briqueta2kg']);
       controllerEscoria = TextEditingController( text: data['escoria']);
       controllerNataCuba = TextEditingController( text: data['natacuba']);
       controllerTiempoLimpieza = TextEditingController( text: data['tiempolimpieza']);
       controllerTiempoParo = TextEditingController( text: data['tiempoparada']);
       controllerCubas = TextEditingController(text: data['cubas']);
       controllerLingotes = TextEditingController(text: data['lingotes']);
       controllerCajas = TextEditingController(text: data['cajas']);
       controllerBriqueta1Dropdown = data['briquetatipo1'];
       controllerBriqueta2Dropdown = data['briquetatipo2'];
    }
    else{
      controllerTurno = 1;
      controllerGasH11 = TextEditingController();
      controllerGasH9  = TextEditingController();
      controllerBriqueta1KG = TextEditingController();
      controllerBriqueta2KG = TextEditingController();
      controllerEscoria = TextEditingController();
      controllerNataCuba = TextEditingController();
       controllerTiempoLimpieza = TextEditingController();
      controllerTiempoParo= TextEditingController();
      controllerCubas = TextEditingController();
      controllerLingotes = TextEditingController();
      controllerCajas = TextEditingController();
    }
    super.initState();
  }
  

  @override
  Widget build(BuildContext context) {
    var datasheetManager = Provider.of<DatasheetManager>(context);
    var buttonmanager = Provider.of<CubasButtonManager>(context);
    var buttonmanagercajas = Provider.of<CajasButtonManager>(context);
    var buttonmanagerlingotes = Provider.of<LingotesButtonManager>(context);

    return DefaultTabController(
      length: 4,
      child: Scaffold(
        key: _scaffoldKey,
        persistentFooterButtons: widget.currentUser.category!='admin'?<Widget>[
        Text("Numero de cubas: ${buttonmanager.cubasCounter.toString()}",
        style: TextStyle(
          color: Colors.white,
          fontSize: 19
        ),
        ),
        Text("Numero de cajas: ${buttonmanagercajas.cajasCounter.toString()}",
        style: TextStyle(
          color: Colors.white,
          fontSize: 19
        ),
        ),
        Text("Numero de lingotes: ${buttonmanagerlingotes.lingotesCounter.toString()}",
        style: TextStyle(
          color: Colors.white,
          fontSize: 19
        ),
        ),
      ]:null,
        backgroundColor: Colors.blue,
        appBar: AppBar(
          title: Text("Entrada de datos"), 
          elevation: 0,
          bottom: TabBar(
            tabs: <Widget>[
              Tab(text: "Cons.",),
              Tab(text: "M.Prima",),
              Tab(text: "T.Limp."),
              Tab(icon:Icon(Icons.check))
            ],
          )
        ),
        body: TabBarView(
          children: <Widget>[
            pantallaConsumos(),
            pantallaMateriaPrima(_scaffoldKey),
            pantallaTiemposLimpieza(),
            pantallaFinalizacion()
          ],
        ),
        floatingActionButton: Builder(
            builder: (context) => myFloatingButton(
                widget.currentUser, selectedDate, context, datasheetManager))
      ),
    );
  }

  Widget myFloatingButton(User currentUser, DateTime selectedDate,
      BuildContext context, DatasheetManager datasheetManager) {
    return FloatingActionButton.extended(
      backgroundColor: Colors.white,
      icon: Icon(Icons.send, color: Colors.blue),
      label: Text("Enviar datos",
          style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold)),
      onPressed: () async {
        if (selectedDate != null) {
          if(widget.editSheet!=null){
            var rightNow = DateTime.now();
            datasheetManager.edit(DataSheet(
              whoUploaded: currentUser.name,
              uploadDate: "${rightNow.year}-${rightNow.month.toString().padLeft(2,'0')}-${rightNow.day.toString().padLeft(2,'0')}",
              uploadTime: "${rightNow.toIso8601String().substring(11, 19)}",
              isFinished: controllerFinalizado,
              data: {
                "fechaRegistro": "${selectedDate.year}-${selectedDate.month.toString().padLeft(2,'0')}-${selectedDate.day.toString().padLeft(2,'0')}",
                "turno": controllerTurno.toString(),
                "gash11":controllerGasH11.text,
                "gash9":controllerGasH9.text,
                "briqueta1tipo": controllerBriqueta1Dropdown,
                "briqueta1kg":controllerBriqueta1KG.text,
                "briqueta2tipo":controllerBriqueta2Dropdown,
                "briqueta2kg":controllerBriqueta2KG.text,
                "escoria":controllerEscoria.text,
                "natacuba":controllerNataCuba.text,
                "timepolimpieza":controllerTiempoLimpieza.text,
                "timepoparada":controllerTiempoParo.text,
                "cajas": controllerCajas.text,
                "cubas": controllerCubas.text,
                "lingotes": controllerLingotes.text
              }), widget.idx);
          }
          Scaffold.of(context).showSnackBar(SnackBar(
            content: RichText(
              text: TextSpan(
                  text: "Los datos se han enviado ",
                  style: TextStyle(color: Colors.white),
                  children: [
                    TextSpan(
                        text: "CORRECTAMENTE",
                        style: TextStyle(
                            color: Colors.blue, fontWeight: FontWeight.bold))
                  ]),
            ),
          ));
          if(widget.editSheet==null){
            var rightNow = DateTime.now();
          datasheetManager.add(DataSheet(
              whoUploaded: currentUser.name,
              uploadDate: "${rightNow.year}-${rightNow.month.toString().padLeft(2,'0')}-${rightNow.day.toString().padLeft(2,'0')}",
              uploadTime: "${rightNow.toIso8601String().substring(11, 19)}",
              isFinished: controllerFinalizado, //CAMBIAR CAMBIAR CAMBIAR
              data: {
                "fechaRegistro": "${selectedDate.year}-${selectedDate.month.toString().padLeft(2,'0')}-${selectedDate.day.toString().padLeft(2,'0')}",
                "turno":controllerTurno.toString(),
                "gash11":controllerGasH11.text,
                "gash9":controllerGasH9.text,
                "briqueta1tipo":controllerBriqueta1Dropdown,
                "briqueta1kg":controllerBriqueta1KG.text,
                "briqueta2tipo":controllerBriqueta2Dropdown,
                "briqueta2kg":controllerBriqueta2KG.text,
                "escoria":controllerEscoria.text,
                "natacuba":controllerNataCuba.text,
                "timepolimpieza":controllerTiempoLimpieza.text,
                "timepoparada":controllerTiempoParo.text,
                "cajas": controllerCajas.text,
                "cubas": controllerCubas.text,
                "lingotes": controllerLingotes.text
              }));
          }
          Navigator.pop(context);
          //SUBIR DATOS A FIRESTORE
        } else {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: RichText(
              text: TextSpan(
                  text: "Se debe seleccionar una ",
                  style: TextStyle(color: Colors.white),
                  children: [
                    TextSpan(
                        text: "FECHA",
                        style: TextStyle(
                            color: Colors.redAccent,
                            fontWeight: FontWeight.bold))
                  ]),
            ),
            duration: Duration(seconds: 3),
          ));
        }
      },
    );
  }

  Widget pantallaConsumos(){
    return SingleChildScrollView(
      child: Padding(
              padding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
              child: Column(
                children: <Widget>[
                  myTextFormField(
                      controller: controllerGasH9,
                      inputType: TextInputType.number,
                      labelText: "Consumo Gas H9"),
                  myTextFormField(
                    controller: controllerGasH11,
                    inputType: TextInputType.number,
                    labelText: "Consumo Gas H11")
                ]
              ),
            ),
    );
  }

  Widget pantallaMateriaPrima(GlobalKey _scaffoldKey) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
             Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Tipo Briqueta 1",
                          style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 600,
                            height: 60,
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                              color: controllerBriqueta1Dropdown==null?Colors.white54:Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(30))
                            ),
                            child: DropdownButton<String>(
              underline: Container(color:Colors.blue),
              icon: Icon(null),
              elevation: 0,
              value: controllerBriqueta1Dropdown,
              hint: Text("Campo pendiente de ser rellenado"),
              items: listBriquetas.map((b){
                return DropdownMenuItem<String>(
                  child: Text(b),
                  value: b,
                );
              }).toList(),
              onChanged: (val){
                setState(() {
                  controllerBriqueta1Dropdown = val;
                });
              },
            ),
                          ),
                        ],
                      ),
                    ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40.0),
                    child: myTextFormField(
                        controller: controllerBriqueta1KG,
                        inputType: TextInputType.number,
                        labelText: "KG Briqueta 1"),
                  ),
                 Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Tipo Briqueta 2",
                          style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Container(
                            width: 600,
                            height: 60,
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                              color: controllerBriqueta2Dropdown==null?Colors.white54:Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(30))
                            ),
                            child: DropdownButton<String>(
              underline: Container(color:Colors.blue),
              icon: Icon(null),
              elevation: 0,
              value: controllerBriqueta2Dropdown,
              hint: Text("Campo pendiente de ser rellenado"),
              items: listBriquetas.map((b){
                return DropdownMenuItem<String>(
                  child: Text(b),
                  value: b,
                );
              }).toList(),
              onChanged: (val){
                setState(() {
                  controllerBriqueta2Dropdown = val;
                });
              },
            ),
                          ),
                        ],
                      ),
                    ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40.0),
                    child: myTextFormField(
                        controller: controllerBriqueta2KG,
                        inputType: TextInputType.number,
                        labelText: "KG Briqueta 2"),
                  ),
                  myTextFormField(
                    controller: controllerCubas,
                    inputType: TextInputType.number,
                    labelText: "Cubas"),
                    myTextFormField(
                    controller: controllerLingotes,
                    inputType: TextInputType.number,
                    labelText: "Lingotes"),
                    myTextFormField(
                    controller: controllerCajas,
                    inputType: TextInputType.number,
                    labelText: "Cajas"),
          ],
        )
      ),
    );
  }

  Widget pantallaTiemposLimpieza() {
    return SingleChildScrollView(
      child: Padding(
              padding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
              child: Column(
                children: <Widget>[
                  myTextFormField(
                      controller: controllerEscoria,
                      inputType: TextInputType.number,
                      labelText: "Cantidad Escoria"),
                  myTextFormField(
                      controller: controllerNataCuba,
                      inputType: TextInputType.number,
                      labelText: "Cantidad Nata Cuba"),
                  myTextFormField(
                      controller: controllerTiempoLimpieza,
                      inputType: TextInputType.number,
                      labelText: "Tiempo Limpieza (min)"),
                  myTextFormField(
                      controller: controllerTiempoParo,
                      inputType: TextInputType.number,
                      labelText: "Tiempo Paro (min)"),
                ]
              ),
            ),
    ); 
  }

  Widget pantallaFinalizacion() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
        child: Column(
          children: <Widget>[
            CheckboxListTile(
                    value: controllerFinalizado,
                    title: Text("Dar como finalizado?", style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),),
                    subtitle: Text("Con la casilla marcada -> Los datos se suben a la nube y se dan como finales. \n Con la casilla desmarcada -> Los datos se guardarán y podrán ser modificados."),
                    onChanged: (value){ setState(() {
                      print(value);
                      controllerFinalizado=value;
                    });
                    },
                    checkColor: Colors.white,
                    activeColor: Colors.green,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          _selectDate(context).then((var value) {
                            setState(() {
                              selectedDate = value;
                            });
                          });
                        },
                        child: Card(
                          color: selectedDate == null
                              ? Colors.redAccent
                              : Colors.green,
                          child: Container(
                              child: Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Text(
                              selectedDate == null
                                  ? "No hay fecha seleccionada"
                                  : "Fecha seleccionada: ${selectedDate.toIso8601String().substring(0, 10)}",
                              style: TextStyle(
                                  color: selectedDate == null
                                      ? Colors.black
                                      : Colors.white),
                            ),
                          )),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Turno 1", style: TextStyle(color: controllerTurno==1?Colors.white:Colors.black)),
                      Radio(
                    groupValue: controllerTurno,
                    value: 1,
                    onChanged: (val) => setState((){
                      controllerTurno=val;
                    }),
                    activeColor: Colors.white
                  ),
                  Text("Turno 2", style: TextStyle(color: controllerTurno==2?Colors.white:Colors.black)),
                  Radio(
                    groupValue: controllerTurno,
                    value: 2,
                    onChanged: (val) => setState((){
                      controllerTurno=val;
                    }),
                    activeColor: Colors.white,
                  ),
                  Text("Turno 3", style: TextStyle(color: controllerTurno==3?Colors.white:Colors.black)),
                  Radio(
                    groupValue: controllerTurno,
                    value: 3,
                    onChanged: (val) => setState((){
                      controllerTurno=val;
                    }),
                    activeColor: Colors.white,
                  ),
                    ],
                  ),
          ],
        ),
      ),
    );
  }
}

Widget myTextFormField(
    {TextEditingController controller,
    TextInputType inputType,
    String labelText}) {
  return Padding(
    padding: const EdgeInsets.only(top: 3, bottom: 3),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(labelText,
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        TextFormField(
            style: TextStyle(color: Colors.black),
            controller: controller,
            keyboardType: inputType,
            decoration: InputDecoration(
              filled: true,
              hintText: controller.text.isEmpty?"Campo pendiente de ser rellenado":null,
              fillColor: controller.text.isEmpty?Colors.white54:Colors.white,
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 0.5, color: Colors.white),
                  borderRadius: BorderRadius.circular(30)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: 2)),
            )),
      ],
    ),
  );
}

Future<DateTime> _selectDate(BuildContext context) {
  return showDatePicker(
    context: context,
    firstDate: DateTime.now().subtract(Duration(days: 7)),
    lastDate: DateTime.now().add(Duration(seconds: 1)),
    initialDate: DateTime.now(),
  );
}