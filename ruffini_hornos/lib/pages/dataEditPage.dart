import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './newdataInputPage.dart';
import '../classes/user.dart';
import '../classes/datasheet.dart';
import '../classes/datasheetmanager.dart';
import 'package:badges/badges.dart';

class DataEditPage extends StatefulWidget {
  @override
  _DataEditPageState createState() => _DataEditPageState();
  DataEditPage(this.currentUser);
  User currentUser;
}

class _DataEditPageState extends State<DataEditPage> {
  @override
  Widget build(BuildContext context) {
    DatasheetManager datasheetManager = Provider.of<DatasheetManager>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Edición y finalización de datos"),
        elevation: 0,
      ),
      backgroundColor: Colors.blue,
      body: ListView.builder(
        itemCount: datasheetManager.list.length,
        itemBuilder: (context, idx){
          return MyCard(idx, datasheetManager);
        },
      )
    );
  }

  Widget MyCard(int idx, DatasheetManager datasheetManager){
    if(datasheetManager.list[idx].isFinished==true){
      return Container();
    } else{
      return Padding(
      padding: const EdgeInsets.fromLTRB(5,1,5,1),
      child: Card(
        child: Container(
          color: Colors.white,
          child: ListTile(
            title: Text("Creación: ${datasheetManager.list[idx].uplaodDate} - ${datasheetManager.list[idx].uploadTime}"),
            subtitle: Text("Creado por: ${datasheetManager.list[idx].whoUploaded}"),
            trailing: Icon(Icons.arrow_right, color: Colors.blue,),
            leading: Badge(
              toAnimate: false,
              elevation: 0,
              badgeContent: Text("Turno ${datasheetManager.list[idx].data['turno']}", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
              badgeColor: Colors.blue,
              shape: BadgeShape.circle,
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 5),
            ),
            onTap: ()=> Navigator.of(context).push(MaterialPageRoute(
              builder: (context)=> NewDataInputPage(widget.currentUser, editSheet: datasheetManager.list[idx], idx: idx,)
            ),)
        ),
      ),
    ),
    );
    }
  }
}