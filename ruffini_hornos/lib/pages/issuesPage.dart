import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:ruffini_hornos/classes/datasheetmanager.dart';
import "../classes/issue.dart";
import "../classes/issuemanager.dart";
import "../classes/user.dart";
import 'package:badges/badges.dart';

class IssuesPage extends StatefulWidget {
  @override
  _IssuesPageState createState() => _IssuesPageState();

  User _currentUser;

  IssuesPage(this._currentUser);
}

class _IssuesPageState extends State<IssuesPage> {
  @override
  Widget build(BuildContext context) {

    var issuesManager = Provider.of<IssueManager>(context);
    var newIssue = Issue();

    return Scaffold(
      appBar: AppBar(
        title: Text("Averías"),
        elevation: 0,
      ),
      backgroundColor: Colors.blue,
      body: ListView.builder(
        itemCount: issuesManager.list.length,
        itemBuilder: (context, idx){
          return MyCard(idx, issuesManager);
        },
      )
    );
  }

  Widget MyCard(int idx, IssueManager issueManager) {
    if(issueManager.list[idx].state=="Finalizada") return Container();
    else {
      return Padding(
        padding: const EdgeInsets.fromLTRB(5, 1, 5, 1),
        child: Card(
          child: Container(
            color: Colors.white,
            child: ExpansionTile(
              initiallyExpanded: false,
              title: Text(issueManager.list[idx].issueType, style: TextStyle(fontWeight: FontWeight.bold),),
              subtitle: Text(
                issueManager.list[idx].issueSubType,
                  style: TextStyle(color: Colors.black),
              ),
              trailing: Icon(Icons.keyboard_arrow_down),
              leading: Badge(
                position: BadgePosition(bottom:0,left:9,top:0,right: 0),
                toAnimate: false,
                elevation: 0,
                badgeColor: Colors.blue,
                badgeContent: Text(issueManager.list[idx].state, style: TextStyle(color: Colors.white, fontSize: 15),),
                shape: BadgeShape.square,
                borderRadius: 15,
              ),
              children: <Widget>[
                RichText(
                text: TextSpan(
                  text: "Se notifico en la fecha: ",
                  style: TextStyle(color: Colors.grey),
                  children: [
                    TextSpan(text:issueManager.list[idx].notificationDate, style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold))
                  ]
                ),
              ),
              Divider(),
              RichText(
                text: TextSpan(
                  text: "Se notificó a la hora: ",
                  style: TextStyle(color: Colors.grey),
                  children: [
                    TextSpan(text:issueManager.list[idx].notificationTime, style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold))
                  ]
                ),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: RichText(
                  text: TextSpan(
                    text: "Fue notifiacada por el usuario: ",
                    style: TextStyle(color: Colors.grey),
                    children: [
                      TextSpan(text:issueManager.list[idx].reporterUser, style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold))
                    ]
                  ),
                ),
              ),
              issueManager.list[idx].state !="Atendiendo" ? Container() :
              Column(children: <Widget>[
                Divider(),
                Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: RichText(
                  text: TextSpan(
                    text: "Se emepezó a atender en la fecha: ",
                    style: TextStyle(color: Colors.grey),
                    children: [
                      TextSpan(text:issueManager.list[idx].attendDate, style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold))
                    ]
                  ),
                ),
              ),
              Divider(),
                Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: RichText(
                  text: TextSpan(
                    text: "Se emepezó a atender a las: ",
                    style: TextStyle(color: Colors.grey),
                    children: [
                      TextSpan(text:issueManager.list[idx].attendTime, style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold))
                    ]
                  ),
                ),
              ),
              Divider(),
                Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: RichText(
                  text: TextSpan(
                    text: "Se está atendiendo por el usuario: ",
                    style: TextStyle(color: Colors.grey),
                    children: [
                      TextSpan(text:issueManager.list[idx].attendantUser, style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold))
                    ]
                  ),
                ),
              ),
              ],)
              ],
            )
          ),
        ),
      );
    }
  }
}




  