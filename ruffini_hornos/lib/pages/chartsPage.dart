import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../classes/datasheetmanager.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class ChartsPage extends StatefulWidget {
  @override
  _ChartsPageState createState() => _ChartsPageState();
}

class _ChartsPageState extends State<ChartsPage> {
  @override
  Widget build(BuildContext context) {

    DatasheetManager datasheetManager = Provider.of<DatasheetManager>(context);
    List<GasData> chartData = [];
    initializeData(datasheetManager, chartData);

    return DefaultTabController(
      length: 1,
      child: Scaffold(
        appBar: AppBar(title: Text("Visualización de datos"),),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SfCartesianChart(
            legend: Legend(isVisible: true),
            primaryXAxis: DateTimeAxis(
            ),
            primaryYAxis: NumericAxis(
              maximum: 1400,
              minimum: 500
            ),
            series: <ChartSeries>[
              LineSeries<GasData,DateTime>(
                width: 4,
                name: "Turno 1",
                color: Colors.blue,
                markerSettings: MarkerSettings(isVisible: true),
                dataSource: chartData.where((gd)=>gd.turno==1).toList(),
                xValueMapper: (GasData data, _) => data.day,
                yValueMapper: (GasData data, _) => data.gas
              ),
              LineSeries<GasData,DateTime>(
                width: 4,
                name: "Turno 2",
                color: Colors.red,
                markerSettings: MarkerSettings(isVisible: true),
                dataSource: chartData.where((gd)=>gd.turno==2).toList(),
                xValueMapper: (GasData data, _) => data.day,
                yValueMapper: (GasData data, _) => data.gas
              ),
              LineSeries<GasData,DateTime>(
                width: 4,
                name: "Turno 3",
                color: Colors.green,
                markerSettings: MarkerSettings(isVisible: true),
                dataSource: chartData.where((gd)=>gd.turno==3).toList(),
                xValueMapper: (GasData data, _) => data.day,
                yValueMapper: (GasData data, _) => data.gas
              )
            ],
          ),
        )
      ),
    );
  }

  void initializeData(DatasheetManager dm, List<GasData>cd){
    dm.list.forEach((doc){
      cd.add(GasData(DateTime.parse(doc.data['fechaRegistro']), int.parse(doc.data['turno']), double.parse(doc.data['gash11']) + double.parse(doc.data['gash9'])));
      }
    );
    cd.sort((gasData1, gasData2){
      return gasData1.day.compareTo(gasData2.day);
    });
    //cd.removeWhere((GasData gd)=>gd.turno!=1);
  }
}

class GasData {
  GasData(this.day, this.turno, this.gas);
  final DateTime day;
  final int turno;
  final double gas;
}