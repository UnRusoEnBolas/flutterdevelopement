import 'package:ruffini_hornos/classes/datasheet.dart';
import 'package:ruffini_hornos/classes/datasheetmanager.dart';
import 'package:ruffini_hornos/pages/issuesPage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../pages/dataEditPage.dart';
import '../pages/issueInputPage.dart';
import '../pages/alertInputPage.dart';
import '../pages/newDataInputPage.dart';
import '../classes/user.dart';
import '../classes/alert.dart';
import '../classes/alertmanager.dart';
import '../classes/cubasbuttonmanager.dart';
import '../classes/lingotesbuttonmanager.dart';
import '../classes/cajasbuttonmanager.dart';
import '../classes/issuemanager.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
  User currentUser;
  MainPage({User user}) {
    currentUser = user;
  }
}

class _MainPageState extends State<MainPage> {
  String filterValue = "TODO";
  String filterValueWorker = "Todos";
  Widget adminFloatingButton() {
    return FloatingActionButton.extended(
      backgroundColor: Colors.blue,
      icon: Icon(Icons.add),
      label: Text("Nueva alerta"),
      /* onPressed: () => Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => AlertInputPage(widget.currentUser))), */
    );
  }

  @override
  Widget build(BuildContext context) {
    var alertManager = Provider.of<AlertManager>(context);
    var buttonmanager;
    var buttonmanagerCajas;
    var buttonmanagerLingotes;
    if (widget.currentUser.category != 'admin') {
      buttonmanager = Provider.of<CubasButtonManager>(context);
      buttonmanager.initialize(widget.currentUser.name);
      buttonmanagerCajas = Provider.of<CajasButtonManager>(context);
      buttonmanagerCajas.initialize(widget.currentUser.name);
      buttonmanagerLingotes = Provider.of<LingotesButtonManager>(context);
      buttonmanagerLingotes.initialize(widget.currentUser.name);
    }
    return Scaffold(
      persistentFooterButtons: widget.currentUser.category != 'admin'
          ? <Widget>[
              Container(
                width: MediaQuery.of(context).size.width * 0.95,
                height: MediaQuery.of(context).size.height * 0.20 ,
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                              text: "Contador de cubas:",
                              style: TextStyle(fontSize: 17),
                              children: [
                                TextSpan(
                                    text:
                                        " ${buttonmanager.cubasCounter.toString()}",
                                    style: TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold))
                              ]),
                        ),
                        Expanded(child: Divider(),),
                        IconButton(
                          color: Colors.white,
                          icon: Icon(Icons.refresh),
                          onPressed: () {
                            buttonmanager.reset();
                          },
                        ),
                        IconButton(
                          color: Colors.white,
                          icon: Icon(Icons.arrow_downward),
                          onPressed: () {
                            buttonmanager.substract();
                          },
                        ),
                        IconButton(
                          color: Colors.white,
                          icon: Icon(Icons.arrow_upward),
                          onPressed: () {
                            buttonmanager.add();
                          },
                        )
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        /* Expanded(
                          child: */
                        RichText(
                          text: TextSpan(
                              text: "Contador de cajas:",
                              style: TextStyle(fontSize: 17),
                              children: [
                                TextSpan(
                                    text:
                                        " ${buttonmanagerCajas.cajasCounter.toString()}",
                                    style: TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold))
                              ]),
                        ),
                        //),
                        Expanded(child: Divider(),),
                        IconButton(
                          color: Colors.white,
                          icon: Icon(Icons.refresh),
                          onPressed: () {
                            buttonmanagerCajas.reset();
                          },
                        ),
                        IconButton(
                          color: Colors.white,
                          icon: Icon(Icons.arrow_downward),
                          onPressed: () {
                            buttonmanagerCajas.substract();
                          },
                        ),
                        IconButton(
                          color: Colors.white,
                          icon: Icon(Icons.arrow_upward),
                          onPressed: () {
                            buttonmanagerCajas.add();
                          },
                        )
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        /* Expanded(
                          child: */
                        RichText(
                          text: TextSpan(
                              text: "Contador de lingotes:",
                              style: TextStyle(fontSize: 17),
                              children: [
                                TextSpan(
                                    text:
                                        " ${buttonmanagerLingotes.lingotesCounter.toString()}",
                                    style: TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold))
                              ]),
                        ),
                        //),
                        Expanded(child: Divider(),),
                        IconButton(
                          color: Colors.white,
                          icon: Icon(Icons.refresh),
                          onPressed: () {
                            buttonmanagerLingotes.reset();
                          },
                        ),
                        IconButton(
                          color: Colors.white,
                          icon: Icon(Icons.arrow_downward),
                          onPressed: () {
                            buttonmanagerLingotes.substract();
                          },
                        ),
                        IconButton(
                          color: Colors.white,
                          icon: Icon(Icons.arrow_upward),
                          onPressed: () {
                            buttonmanagerLingotes.add();
                          },
                        )
                      ],
                    ),
                  ],
                ),
              )
            ]
          : null,
      appBar: AppBar(
          centerTitle: true,
          bottom: null,
          backgroundColor: Colors.blue,
          elevation: 2,
          title: PreferredSize(
            preferredSize: const Size.fromHeight(48.0),
            child: Theme(
              data: Theme.of(context).copyWith(accentColor: Colors.white),
              child: Container(
                height: 48.0,
                alignment: Alignment.center,
                child: Theme(
                  data: Theme.of(context).copyWith(canvasColor: Colors.blue),
                  child: DropdownButton<String>(
                    underline: Container(color: Colors.blue),
                    icon: Icon(
                      Icons.arrow_drop_down,
                      color: Colors.white,
                    ),
                    elevation: 0,
                    value: filterValue,
                    hint: Text("Filtro"),
                    items: [
                      DropdownMenuItem<String>(
                          value: "TODO",
                          child: Text(
                            "Alertas: Todo",
                            style: TextStyle(color: Colors.white, fontSize: 19),
                          )),
                      DropdownMenuItem<String>(
                        value: "BRIQUETAS",
                        child: Text(
                          "Alertas: Briquetas",
                          style: TextStyle(color: Colors.white, fontSize: 19),
                        ),
                      ),
                      DropdownMenuItem<String>(
                          value: "CONSUMO",
                          child: Text(
                            "Alertas: Consumo",
                            style: TextStyle(color: Colors.white, fontSize: 19),
                          )),
                      DropdownMenuItem<String>(
                          value: "AVERIAS",
                          child: Text(
                            "Alertas: Averias",
                            style: TextStyle(color: Colors.white, fontSize: 19),
                          )),
                      DropdownMenuItem<String>(
                          value: "MAT. PRIMA",
                          child: Text(
                            "Alertas: Materia prima",
                            style: TextStyle(color: Colors.white, fontSize: 19),
                          )),
                      DropdownMenuItem<String>(
                          value: "ESCORIA",
                          child: Text(
                            "Alertas: Escoria",
                            style: TextStyle(color: Colors.white, fontSize: 19),
                          )),
                      DropdownMenuItem<String>(
                          value: "OTRAS",
                          child: Text(
                            "Alertas: Otras",
                            style: TextStyle(color: Colors.white, fontSize: 19),
                          )),
                    ],
                    onChanged: (val) {
                      setState(() {
                        filterValue = val;
                      });
                    },
                  ),
                ),
              ),
            ),
          )),
      backgroundColor: Colors.blue,
      drawer: myDrawer(widget.currentUser, context),
      body: ListView.builder(
        itemCount: alertManager.list.length,
        itemBuilder: (context, idx) {
          Alert thisAlert = alertManager.list[idx];
          if (thisAlert.category == filterValue || filterValue == "TODO") {
            return MyCard(
                idx, thisAlert, alertManager, widget.currentUser.name);
          } else {
            return Container();
          }
        },
      ),
      floatingActionButton:
          widget.currentUser.category == "admin" ? adminFloatingButton() : null,
    );
  }
}

class MyCard extends StatelessWidget {
  String userName;
  Alert alert;
  int index;
  AlertManager alertManager;
  MyCard(this.index, this.alert, this.alertManager, this.userName);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(5.0, 1.0, 5.0, 1.0),
      child: Card(
          child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 8, 10, 8),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(alert.publishedDate,
                    style: TextStyle(fontSize: 13, color: Colors.grey)),
                Text(alert.publishedTime,
                    style: TextStyle(fontSize: 13, color: Colors.grey)),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8, top: 2),
              child: Text(
                alert.title,
                style: TextStyle(fontSize: 21, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text(
                alert.body,
                style: TextStyle(fontSize: 15),
                textAlign: TextAlign.justify,
              ),
            ),
            messageCategory(alert),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Divider(
                height: 1,
                thickness: 2,
              ),
            ),
            Column(
              children: <Widget>[
                alert.hasBeenChecked
                    ? Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                        child: Text(
                          "Asignada a: ${alert.whoChecked}",
                          style: TextStyle(fontSize: 13, color: Colors.grey),
                        ))
                    : IconButton(
                        icon: Icon(
                          Icons.add,
                          color: Colors.blue,
                          size: 35,
                        ),
                        onPressed: () {
                          alertManager.setAsChecked(index, userName);
                          Scaffold.of(context).showSnackBar(SnackBar(
                              content: RichText(
                                text: TextSpan(
                                    text: "Te has asigando la tarea: ",
                                    style: TextStyle(color: Colors.white),
                                    children: [
                                      TextSpan(
                                          text: "${alert.title}",
                                          style: TextStyle(
                                              color: Colors.blue,
                                              fontWeight: FontWeight.bold))
                                    ]),
                              ),
                              duration: Duration(seconds: 4),
                              action: SnackBarAction(
                                  textColor: Colors.redAccent,
                                  label: "DESHACER",
                                  onPressed: () =>
                                      alertManager.undoCheck(index))));
                        }),
                alert.whoChecked == userName
                    ? TextField(
                        controller: alertManager.list[index].comment == null
                            ? null
                            : TextEditingController(
                                text: alertManager.list[index].comment),
                        onSubmitted: (comment) {
                          alertManager.addComment(index, comment);
                        },
                        style: TextStyle(fontSize: 13),
                        decoration: InputDecoration(
                            icon: Icon(Icons.comment),
                            border: OutlineInputBorder(),
                            hintText: "Añade un comentario sobre la tarea..."),
                      )
                    : Container(
                        child: Text(
                          alertManager.list[index].comment == null
                              ? ''
                              : alertManager.list[index].comment,
                          style: TextStyle(),
                        ),
                      )
              ],
            ),
          ],
        ),
      )),
    );
  }
}

Widget myDrawer(User currentUser, BuildContext context) {
  DatasheetManager datasheetManager = Provider.of<DatasheetManager>(context);
  IssueManager issueManager = Provider.of<IssueManager>(context);
  String numberNotFinishedDatasheets =
      datasheetManager.nNotFinished().toString();
  String numberNotFinishedIssues = issueManager.nNotFinished().toString();
  return Drawer(
    elevation: 0,
    child: Container(
      color: Colors.blue,
      child: Column(
        children: <Widget>[
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(color: Colors.white),
            accountEmail: Text(currentUser.category,
                style: TextStyle(color: Colors.blue)),
            accountName: Text(
              currentUser.name,
              style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
          ),
          ListTile(
            title: Text(
              "Introducción de datos",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 20),
            ),
            subtitle: Text(
              "Registra los datos medinate un formulario",
              style: TextStyle(color: Colors.white),
            ),
            trailing: Icon(Icons.insert_chart, color: Colors.white),
            onTap: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => NewDataInputPage(currentUser))),
          ),
          ListTile(
              title: Text(
                "Edición/Finalización de datos",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 20),
              ),
              subtitle: Text(
                "Editar o finalizar datos introducidos anteriormente",
                style: TextStyle(color: Colors.white),
              ),
              trailing: SizedBox(
                height: 24,
                width: 24,
                child: Container(
                    decoration: BoxDecoration(
                        color: numberNotFinishedDatasheets == '0'
                            ? Colors.transparent
                            : Colors.redAccent,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(3)),
                    child: Text(
                      numberNotFinishedDatasheets,
                      style: TextStyle(color: Colors.white, fontSize: 20),
                      textAlign: TextAlign.center,
                    )),
              ),
              onTap: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => DataEditPage(currentUser)))),
          Divider(
            color: Colors.white,
            thickness: 0.7,
          ),
          ListTile(
            title: Text(
              "Introducción de averias",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 20),
            ),
            subtitle: Text(
              "Informa al equipo de mantenimiento sobre una avería",
              style: TextStyle(color: Colors.white),
            ),
            trailing: Icon(
              Icons.notification_important,
              color: Colors.white,
              size: 24,
            ),
            onTap: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => IssueInputPage(currentUser))),
          ),
          ListTile(
            title: Text(
              "Estado de averías",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 20),
            ),
            subtitle: Text(
              "Obtén información sobre la averías abiertas",
              style: TextStyle(color: Colors.white),
            ),
            trailing: SizedBox(
              height: 24,
              width: 24,
              child: Container(
                  decoration: BoxDecoration(
                      color: numberNotFinishedIssues == '0'
                          ? Colors.transparent
                          : Colors.redAccent,
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(3)),
                  child: Text(
                    numberNotFinishedIssues,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                    textAlign: TextAlign.center,
                  )),
            ),
            onTap: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => IssuesPage(currentUser))),
          ),
        ],
      ),
    ),
  );
}

Widget messageCategory(Alert alert) {
  Color colorToUse;
  switch (alert.category) {
    case "BRIQUETAS":
      colorToUse = Color(0xFF926C00);
      break;
    case "CONSUMO":
      colorToUse = Color(0xFFD65DB1);
      break;
    case "AVERIAS":
      colorToUse = Color(0xFFFF9671);
      break;
    case "MAT. PRIMA":
      colorToUse = Color(0xFFC34A36);
      break;
    case "ESCORIA":
      colorToUse = Color(0xFF0089BA);
      break;
    case "OTRAS":
      colorToUse = Color(0xFFB2B2B2);
  }
  return Container(
      padding: EdgeInsets.all(3),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(3), color: colorToUse),
      child: Text(
        alert.category.toString().toUpperCase(),
        style: TextStyle(color: Colors.white, fontSize: 13),
      ));
}
