import 'package:ruffini_hornos/classes/datasheetmanager.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './classes/alertmanager.dart';
import './classes/cubasbuttonmanager.dart';
import './classes/cajasbuttonmanager.dart';
import './classes/lingotesbuttonmanager.dart';
import './classes/issuemanager.dart';
import 'pages/loginPage.dart';

void main(){
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(builder: (_) => AlertManager()),
        ChangeNotifierProvider(builder: (_) => DatasheetManager()),
        ChangeNotifierProvider(builder: (_) => CubasButtonManager()),
        ChangeNotifierProvider(builder: (_) => LingotesButtonManager()),
        ChangeNotifierProvider(builder: (_) => CajasButtonManager()),
        ChangeNotifierProvider(builder: (_) => IssueManager())
      ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
        theme: ThemeData(fontFamily: 'ProductSans'),
        home: LoginPage()
      ),
    )
  );
}