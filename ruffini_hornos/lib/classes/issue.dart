import 'package:cloud_firestore/cloud_firestore.dart';

class Issue{
  String _id;

  String _notificationTime;
  String _notificationDate; 
  String _attendTime;
  String _attendDate; 
  String _finishTime;
  String _finishDate;

  String _issueType;
  String _issueSubType;

  String _reporterUser;
  String _attendantUser;

  String _state; // NotificationEmmited - Attending - Delivered - Finished

  set id(id) => _id = id;
  get id => _id;
  get notificationTime => _notificationTime;
  get notificationDate => _notificationDate;
  get attendTime => _attendTime;
  get attendDate => _attendDate;
  get finishTime => _finishTime;
  get finishDate => _finishDate;
  get issueType => _issueType;
  get issueSubType => _issueSubType;
  get reporterUser => _reporterUser;
  get attendantUser => _attendantUser;
  get state => _state;

  Issue(
    {String notificationTime, String notificationDate, String attendTime, String attendDate, String deliveryTime, String deliveryDate,
    String finishTime, String finishDate, bool checkedFinishedOvens, bool checkedFinishedMaintenance,
    bool isCompleteyFinished, String issueType, String issueSubtype, String reporterUser,
    String attendantUser, String state}) {
    _notificationTime = notificationTime;
    _notificationDate = notificationDate;
    _attendTime = attendTime;
    _attendDate = attendDate;
    _finishTime = finishTime;
    _finishDate = finishDate;
    _issueType = issueType;
    _issueSubType = issueSubtype;
    _reporterUser = reporterUser;
    _attendantUser = attendantUser;
    _state = state;
  }

  Issue.fromSnapshot(DocumentSnapshot s){
    var data = s.data;
    _id = s.documentID;
    _notificationTime = data['notificationTime'];
    _notificationDate = data['notificationDate'];
    _attendTime = data['attendTime'];
    _attendDate = data['attendDate'];
    _finishTime = data['finishTime'];
    _finishDate = data['finishDate'];
    _issueType = data['issueType'];
    _issueSubType = data['issueSubType'];
    _reporterUser = data['reporterUser'];
    _attendantUser = data['attendantUser'];
    _state = data['state'];
  }

  toJson(){
    return {
      "notificationTime" : _notificationTime,
      'notificationDate' : _notificationDate,
      "attendTime" : _attendTime,
      'attendDate' : _attendDate,
      "finishTime" : _finishTime,
      'finishDate' : _finishDate,
      "issueType" : _issueType,
      "issueSubType" : _issueSubType,
      "reporterUser" : _reporterUser,
      "attendantUser" : _attendantUser,
      "state" : _state
    };
  }

  editFromNewSnapshot(DocumentSnapshot s){
    var data = s.data;
    _notificationTime = data['notificationTime'];
    _notificationDate = data['notificationDate'];
    _attendTime = data['attendTime'];
    _attendDate = data['attendDate'];
    _finishTime = data['finishTime'];
    _finishDate = data['finishDate'];
    _issueType = data['issueType'];
    _issueSubType = data['issueSubType'];
    _reporterUser = data['reporterUser'];
    _attendantUser = data['attendantUser'];
    _state = data['state'];
  }

  void mainteanceCheckAttend(){
    _attendTime = DateTime.now().toIso8601String().substring(11,19);
    _state = "Siendo atendida";
    
  }
}