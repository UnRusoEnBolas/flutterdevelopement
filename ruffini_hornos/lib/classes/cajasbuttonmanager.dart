import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CajasButtonManager with ChangeNotifier{
  int _cajasCounter;
  String _userName;
  DocumentReference _docRef;

  CajasButtonManager();

  int get cajasCounter => _cajasCounter;

  initialize(String username) async {
    _userName=username;
    QuerySnapshot docReference = await Firestore.instance.collection('Users').where('name', isEqualTo: username).getDocuments(); 
    String docId = docReference.documents[0].documentID;
    _docRef=Firestore.instance.collection('Users').document(docId);
    _docRef.get().then((docSnap){
      _cajasCounter = docSnap.data['cajasCounter'];
    });
    notifyListeners();
  }

  add(){
    _cajasCounter++;
    _docRef.setData({
      'cajasCounter':_cajasCounter
    },
    merge: true
    );
    notifyListeners();
    print(_cajasCounter);
  }

  substract(){
    _cajasCounter--;
    if(_cajasCounter < 0) _cajasCounter=0;
    _docRef.setData({
      'cajasCounter':_cajasCounter
    },
    merge: true
    );
    notifyListeners();
    print(_cajasCounter);
  }

  reset(){
    _cajasCounter=0;
    _docRef.setData({
      'cajasCounter':_cajasCounter
    },
    merge: true
    );
    notifyListeners();
    print(_cajasCounter);
  }
}