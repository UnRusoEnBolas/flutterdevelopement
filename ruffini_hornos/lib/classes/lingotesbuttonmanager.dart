import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class LingotesButtonManager with ChangeNotifier{
  int _lingotesCounter;
  String _userName;
  DocumentReference _docRef;

  LingotesButtonManager();

  int get lingotesCounter => _lingotesCounter;

  initialize(String username) async {
    _userName=username;
    QuerySnapshot docReference = await Firestore.instance.collection('Users').where('name', isEqualTo: username).getDocuments(); 
    String docId = docReference.documents[0].documentID;
    _docRef=Firestore.instance.collection('Users').document(docId);
    _docRef.get().then((docSnap){
      _lingotesCounter = docSnap.data['lingotesCounter'];
    });
    notifyListeners();
  }

  add(){
    _lingotesCounter++;
    _docRef.setData({
      'lingotesCounter':_lingotesCounter
    },
    merge: true
    );
    notifyListeners();
    print(_lingotesCounter);
  }

  substract(){
    _lingotesCounter--;
    if(_lingotesCounter < 0) _lingotesCounter=0;
    _docRef.setData({
      'lingotesCounter':_lingotesCounter
    },
    merge: true
    );
    notifyListeners();
    print(_lingotesCounter);
  }

  reset(){
    _lingotesCounter=0;
    _docRef.setData({
      'lingotesCounter':_lingotesCounter
    },
    merge: true
    );
    notifyListeners();
    print(_lingotesCounter);
  }
}