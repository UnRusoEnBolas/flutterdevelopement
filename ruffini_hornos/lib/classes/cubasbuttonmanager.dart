import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CubasButtonManager with ChangeNotifier{
  int _cubasCounter;
  String _userName;
  DocumentReference _docRef;

  CubasButtonManager();

  int get cubasCounter => _cubasCounter;

  initialize(String username) async {
    _userName=username;
    QuerySnapshot docReference = await Firestore.instance.collection('Users').where('name', isEqualTo: username).getDocuments(); 
    String docId = docReference.documents[0].documentID;
    _docRef=Firestore.instance.collection('Users').document(docId);
    _docRef.get().then((docSnap){
      _cubasCounter = docSnap.data['cubasCounter'];
    });
    notifyListeners();
  }

  add(){
    _cubasCounter++;
    _docRef.setData({
      'cubasCounter':_cubasCounter
    },
    merge: true
    );
    notifyListeners();
    print(_cubasCounter);
  }

  substract(){
    _cubasCounter--;
    if(_cubasCounter < 0) _cubasCounter=0;
    _docRef.setData({
      'cubasCounter':_cubasCounter
    },
    merge: true
    );
    notifyListeners();
    print(_cubasCounter);
  }

  reset(){
    _cubasCounter=0;
    _docRef.setData({
      'cubasCounter':_cubasCounter
    },
    merge: true
    );
    notifyListeners();
    print(_cubasCounter);
  }
}