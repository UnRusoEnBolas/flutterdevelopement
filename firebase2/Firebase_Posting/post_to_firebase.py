from firebase import firebase
import datetime

categoryList = ["BRIQUETAS", "CONSUMO", "AVERIAS", "MATERIA PRIMA", "ESCORIA"]
firebase = firebase.FirebaseApplication("https://flutter-apps-ae161.firebaseio.com/", None)

print("   1- Briquetas\n   2- Consumo\n   3- Averias\n   4- Materia Prima\n   5- Escoria \n\n\n")
category_idx = int(input("Select a category for the message: "))

title = input("Insert a title for the message: ")
message = input("Insert the message body: ")
date = datetime.datetime.now().strftime('%d/%m/%Y')
time = datetime.datetime.now().strftime('%X')

message = {
    'category' : categoryList[category_idx-1],
    'message' : message,
    'date' : date,
    'time' : time,
    'title' : title
}

result = firebase.post("Messages", message)
print(result)