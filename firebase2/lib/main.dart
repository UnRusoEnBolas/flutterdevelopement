import './paginaprincial.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      title: "Notificaciones Hornos",
      debugShowCheckedModeBanner: false,
      home: PaginaPrincipal(),
    )
  );
}