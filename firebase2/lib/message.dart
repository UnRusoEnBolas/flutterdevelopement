import 'package:firebase_database/firebase_database.dart';

class Message {
  String key;
  String date;
  String time;
  String message;
  String category;
  String title;

  Message(this.message, this.category, this.date, this.title);

  Message.fromSnapshot(DataSnapshot snapshot){
    this.key = snapshot.key;
    this.message = snapshot.value['message'];
    this.category = snapshot.value['category'];
    this.title = snapshot.value['title'];
    this.date = snapshot.value['date'];
    this.time = snapshot.value['time'];
  }

  toJson() {
    return {
      "message" : this.message,
      "category" : this.category,
      "date" : this.date,
      "time" : this.time,
      "title" : this.title
    };
  }
}