import 'package:flutter/material.dart';
import 'message.dart';
import 'package:firebase_database/firebase_database.dart';

class PageFirebaseListAll extends StatefulWidget {
  @override
  _PageFirebaseListAllState createState() => _PageFirebaseListAllState();
}

class _PageFirebaseListAllState extends State<PageFirebaseListAll> {
  List<Message> _messagesList = List();
  List<Message> _reversedMessageList = List();
  DatabaseReference dbReference;
  FirebaseDatabase firebaseDB = FirebaseDatabase(
      databaseURL: "https://flutter-apps-ae161.firebaseio.com/");

  @override
  void initState() {
    super.initState();
    dbReference = firebaseDB.reference().child("Messages");
    dbReference.onChildAdded.listen(_onMessageAdded);
  }

  @override
  Widget build(BuildContext context) {
    return ScrollConfiguration(
      behavior: ScrollBehavior(),
      child: GlowingOverscrollIndicator(
        color: Colors.redAccent,
        axisDirection: AxisDirection.down,
        child: ListView.builder(
          itemCount: _reversedMessageList.length,
          itemBuilder: ((BuildContext context, int index) {
            return Card(
                color: Colors.redAccent,
                child: ListTile(
                    title: Text(
                      _reversedMessageList[index].title,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 21),
                    ),
                    subtitle: Text(
                      _reversedMessageList[index].message,
                      style: TextStyle(color: Colors.white),
                      textAlign: TextAlign.justify,
                    ),
                    trailing: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(_reversedMessageList[index].time,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 18)),
                        Text(_reversedMessageList[index].date,
                            style: TextStyle(color: Colors.white, fontSize: 12))
                      ],
                    )));
          }),
        ),
      ),
    );
  }

  /*@override
  Widget build(BuildContext context) {
    return FirebaseAnimatedList(
      defaultChild: Center(
        child: CircularProgressIndicator(),
      ),
      query: dbReference.orderByChild("category").equalTo(widget.filter),
      itemBuilder: (BuildContext context, DataSnapshot snapshot,
          Animation<double> animation, int index) {
            Message msg = Message.fromSnapshot(snapshot);
        return Card(
            color: Colors.redAccent,
            child: ListTile(
                title: Text(
                  msg.category,
                  style: TextStyle(color: Colors.white),
                ),
                subtitle: Text(msg.message,
                    style: TextStyle(color: Colors.white)),
                trailing: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(msg.time,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 18)),
                    Text(msg.date,
                        style: TextStyle(color: Colors.white, fontSize: 12))
                  ],
                )));
      },
    );
  }*/

  void _onMessageAdded(Event event) {
    setState(() {
      _messagesList.add(Message.fromSnapshot(event.snapshot));
      _reversedMessageList = _messagesList.reversed.toList();
    });
  }
}
