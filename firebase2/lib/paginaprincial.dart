import 'package:flutter/material.dart';
import 'paginalistafirebasefilter.dart';
import 'paginelistafirebaseall.dart';

List<Tab> _buttonsNames = [
  Tab(
    child: Align(
      alignment: Alignment.center,
      child: Text("TODO")
    )
  ),
  Tab(
    child: Align(
      alignment: Alignment.center,
      child: Text("BRIQUETAS")
    )
  ),
  Tab(
    child: Align(
      alignment: Alignment.center,
      child: Text("CONSUMO")
    )
  ),
  Tab(
    child: Align(
      alignment: Alignment.center,
      child: Text("AVERIAS")
    )
  ),
  Tab(
    child: Align(
      alignment: Alignment.center,
      child: Text("MAT. PRIMA")
    )
  ),
  Tab(
    child: Align(
      alignment: Alignment.center,
      child: Text("ESCORIA")
    )
  ),
];

class PaginaPrincipal extends StatefulWidget {
  @override
  _PaginaPrincipalState createState() => _PaginaPrincipalState();
}

class _PaginaPrincipalState extends State<PaginaPrincipal>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
        initialIndex: 0, length: _buttonsNames.length, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          backgroundColor: Colors.redAccent,
          title: Text(
            "Notificaciones Hornos",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 27,
                color: Colors.white),
          ),
          bottom: TabBar(
            labelColor: Colors.redAccent,
            unselectedLabelColor: Colors.white,
            indicatorSize: TabBarIndicatorSize.tab,
            indicator: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
              ),
              color: Colors.white
            ),
            isScrollable: true,
            controller: _tabController,
            tabs: _buttonsNames,
          )),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          PageFirebaseListAll(),
          PageFirebaseListFilter(filter: "BRIQUETAS",),
          PageFirebaseListFilter(filter: "CONSUMO"),
          PageFirebaseListFilter(filter: "AVERIAS",),
          PageFirebaseListFilter(filter: "MATERIA PRIMA",),
          PageFirebaseListFilter(filter: "ESCORIA"),
        ],
      )
    );
  }
}