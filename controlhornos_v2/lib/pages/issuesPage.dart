import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:flutter_picker/flutter_picker.dart';
import "../classes/issue.dart";
import "../classes/issuemanager.dart";
import "../classes/user.dart";

class IssuesPage extends StatefulWidget {
  @override
  _IssuesPageState createState() => _IssuesPageState();

  User _currentUser;

  IssuesPage(this._currentUser);
}

class _IssuesPageState extends State<IssuesPage> {
  @override
  Widget build(BuildContext context) {

    var issuesManager = Provider.of<IssueManager>(context);
    var newIssue = Issue();

    return Scaffold(
      appBar: AppBar(
        title: Text("Averías"),
        elevation: 0,
      ),
      backgroundColor: Colors.blue,
      floatingActionButton: FloatingActionButton.extended(
        label: Text("Notificar avería"),
        icon: Icon(Icons.add),
        onPressed: ()=>Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddNewIssue(newIssue))),
      ),
    );
  }
}

class AddNewIssue extends StatefulWidget {
  @override
  _AddNewIssueState createState() => _AddNewIssueState();

  Issue newIssue;
  AddNewIssue(this.newIssue);
}

class _AddNewIssueState extends State<AddNewIssue> {
  
  Map<dynamic,dynamic> selectedIssueObject;
  bool hasBeenSelectedIssue = false;
  bool hasBeenSelectedSubIssue = false;
  String selectedIssue = '';
  String selectedSubIssue = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
        label: Text("Notificar avería",
        style: TextStyle(color: Colors.blue, fontWeight: FontWeight.bold)
        ),
        icon: Icon(Icons.notification_important, color: Colors.blue),
        backgroundColor: Colors.white,
        onPressed: (){
          print("Button pressed!");
        },
      ),
      backgroundColor: Colors.blue,
      appBar: AppBar(
        title: Text("Notificar avería"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Text("1.- Selecciona la avería:",
              style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              myButtonFirst(this),
              selectedIssue=="OTRO"?TextFormField(
            style: TextStyle(color: Colors.black),
            decoration: InputDecoration(
              filled: true,
              hintText: "Introduce una avería personalizada",
              fillColor: Colors.white,
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 0.5, color: Colors.white),
                  borderRadius: BorderRadius.circular(30)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: 2)),
            ),
            onChanged: (txt){
                selectedIssue=txt.toUpperCase();
              }
  ):Container(height: 0),
              Divider(thickness: 1, color: Colors.white),
              Text("2.- Detalla la avería:",
              style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              myButtonSecond(this),
              selectedSubIssue=="OTROS"?TextFormField(
            style: TextStyle(color: Colors.black),
            decoration: InputDecoration(
              filled: true,
              hintText: "Detalla la avería",
              fillColor: Colors.white,
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 0.5, color: Colors.white),
                  borderRadius: BorderRadius.circular(30)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: 2)),
            ),
            onChanged: (txt){
                selectedSubIssue=txt.toUpperCase();
              }
  ):Container(height: 0)
            ],
          ),
        ),
      )
    );
  }

}

class myButtonFirst extends StatelessWidget {

    Future<QuerySnapshot> _data = Firestore.instance.collection("IssuesSystemData").getDocuments();
    _AddNewIssueState _parent;

    myButtonFirst(this._parent);

    @override
    Widget build(BuildContext context) {
      return RaisedButton(
        child: Text(!_parent.hasBeenSelectedIssue?"Seleccionar avería":_parent.selectedIssue),
        color: Colors.white,

        onPressed: (){
          showBottomSheet(
            shape:  RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
            context: context,
            builder: (context){
              return FutureBuilder(
                future: _data,
                builder: (context, snap) {
                  return snap.hasData ?
                  Container(height: 450,
                  child: Column(
                    children: <Widget>[
                      Container(height: 30,
                      child: Icon(Icons.keyboard_arrow_down),
                      ),
                      Container(
                        height: 420,
                      child:ListView.builder(
                      itemCount: snap.data.documents.length,
                      itemBuilder: (context, idx) {
                        return ListTile(
                          title: Text(snap.data.documents[idx].data['name']),
                          subtitle: Text(snap.data.documents[idx].data['code']),
                          trailing: Icon(Icons.keyboard_arrow_right),
                          onTap: (){
                            _parent.setState(() {
                              _parent.selectedIssueObject = snap.data.documents[idx].data;
                              _parent.selectedIssue = _parent.selectedIssueObject['name'];
                              _parent.hasBeenSelectedIssue = true;
                              _parent.selectedSubIssue='';
                              _parent.hasBeenSelectedSubIssue=false;
                              Navigator.of(context).pop();
                            });
                          },
                        );
                      }
                    )
                      )
                    ],
                  ),
                  ) 
                  :
                    CircularProgressIndicator();
                  }
              );
            }
          );
        }
      );
    }
  }

  class myButtonSecond extends StatelessWidget {

    _AddNewIssueState _parent;

    myButtonSecond(this._parent);

    @override
    Widget build(BuildContext context) {
      return RaisedButton(
        
        child: Text(!_parent.hasBeenSelectedSubIssue?"Seleccionar detalle":_parent.selectedSubIssue),
        color: Colors.white,

        onPressed: (){
          _parent.hasBeenSelectedIssue?
          showBottomSheet(
            shape:  RoundedRectangleBorder(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
            context: context,
            builder: (context){
              return Container(height: 450,
                  child: Column(
                    children: <Widget>[
                      Container(height: 30,
                      child: Icon(Icons.keyboard_arrow_down),
                      ),
                      Container(
                        height: 420,
                      child:ListView.builder(
                      itemCount: _parent.selectedIssueObject['children'].length,
                      itemBuilder: (context, idx) {
                        return ListTile(
                          title: Text(_parent.selectedIssueObject['children'][idx].toUpperCase()),
                          subtitle: Text((idx+1).toString()),
                          trailing: Icon(Icons.keyboard_arrow_right),
                          onTap: (){
                            _parent.setState(() {
                              _parent.selectedSubIssue = _parent.selectedIssueObject['children'][idx].toUpperCase();
                              _parent.hasBeenSelectedSubIssue = true;
                              Navigator.of(context).pop();
                            });
                          },
                        );
                      }
                    )
                      )
                    ],
                  ),
                  );
            }
          )
          :
          Scaffold.of(context).showSnackBar(SnackBar(content: Text("Selecciona una avería primero!"),));
        }
      );
    }
  }


  