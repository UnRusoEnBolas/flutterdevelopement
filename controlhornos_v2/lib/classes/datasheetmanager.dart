import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './datasheet.dart';

class DatasheetManager with ChangeNotifier{
  List<DataSheet> _list = [];
  CollectionReference _datasheetsReference;

  DatasheetManager(){
    _datasheetsReference = Firestore.instance.collection('Data');
    _datasheetsReference.snapshots().listen((data){
      data.documentChanges.forEach((change){
        switch(change.type.index){
          case 0:
            print("MyDebug ===> Se ha añadido un Datasheet");
            _list.insert(0,DataSheet.fromSnapshot(change.document));
            break;
          case 1:
            print("MyDebug ===> Se ha modificado un Datasheet");
            int idx = _list.indexWhere((item)=>item.id==change.document.documentID);
            _list[idx].editFromNewSnapshot(change.document);
            break;
          case 2:
            print("MyDebug ===> Se ha eliminado un Datasheet");
            _list.removeWhere((item)=>item.id==change.document.documentID);
            break;
        }
        notifyListeners();
      });
    });
  }

  List<DataSheet> get list => _list;
  void add(DataSheet d) {
    //_list.add(d);
    _datasheetsReference.add(d.toJson());  
    notifyListeners();
  }

  void edit(DataSheet d, idx){
    _datasheetsReference.document(list[idx].id).updateData(d.toJson());
    notifyListeners();
  }

  void setAsFinished(int idx){
    print("MyDebug ===> Se ha marcado un Datasheet como finalizado");
    _list[idx].setAsFinished();
    _datasheetsReference.document(_list[idx].id).updateData(_list[idx].toJson());
    notifyListeners();
  }

  int nNotFinished(){
    return _list.where((sheet)=>sheet.isFinished==false).length;
  }
}