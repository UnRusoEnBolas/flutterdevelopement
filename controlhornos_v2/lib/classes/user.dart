import 'package:cloud_firestore/cloud_firestore.dart';

class User{
  String _name;
  String _code;
  String _category;

  User(this._name, this._code);

  get name => _name;
  get code => _code;
  get category => _category;

  User.fromSnapshot(DocumentSnapshot s){
    _name = s.data['name'];
    _code = s.data['code'];
    _category = s.data['category'];
  }

  toJson(){
    return {
      "name" : _name,
      "code" : _code,
      "category" : _category
    };
  }

  Future<bool> doesExist() async {
    bool isValid;
    QuerySnapshot snapshot = await Firestore.instance.collection('Users')
      .where('name', isEqualTo: _name).getDocuments();

    if (snapshot.documents.length == 0 || snapshot.documents.first.data['code'] != _code){
      isValid = false;
    } else {
      isValid = true;
      _name = snapshot.documents.first.data['name'];
      _code = snapshot.documents.first.data['code'];
      _category = snapshot.documents.first.data['category'];
    }
    return isValid;
  }
}