import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart' as prefix0;
import './alert.dart';

class AlertManager with ChangeNotifier{
  List<Alert> _list = [];
  CollectionReference _alertsReference;
  CollectionReference _usersReference;

  AlertManager(){
    _alertsReference = Firestore.instance.collection("Alerts");
    _alertsReference.snapshots().listen((data){
      data.documentChanges.forEach((change) {
        if( true/*DateTime.parse(change.document.data['publishedDate']).difference(DateTime.now()).inDays < 3*/){
          switch(change.type.index){
          case 0: //añadido
           //print("MyDebug ===> Se ha añadido una alerta");
            _list.insert(0,Alert.fromSnapshot(change.document));
            break;
          case 1: //modificado
            //print("MyDebug ===> Se ha modificado una alerta");
            int idx = _list.indexWhere((item)=>item.id==change.document.documentID);
            _list[idx].editFromNewSnapshot(change.document);
           break;
          case 2: //eliminado
            //print("MyDebug ===> Se ha eliminado una alerta!");
            _list.removeWhere((item)=>item.id == change.document.documentID);
            break;
        }
        print("MyDebug ===> ${_list.toString()}");
        notifyListeners();
        }
      });
      print("MyDebug ===> Not sorted: ${_list.toString()}");
      _list.sort((a,b){
        var i = DateTime.parse("${a.publishedDate} ${a.publishedTime}");
        var j = DateTime.parse("${b.publishedDate} ${b.publishedTime}");
        return i.compareTo(j);
      });
      print("MyDebug ===> Sorted: ${_list.toString()}");
      var aux = _list.reversed.toList();
      _list = aux;
      notifyListeners();
    });
  }

  List<Alert> get list => _list;
  void add(Alert a){
    _list.add(a);
    notifyListeners();
  }

  void addNew(Alert a) {
    //_list.add(d);
    _alertsReference.add(a.toJson());  
    notifyListeners();
  }

  void setAsChecked(int idx, String userName){
    print("MyDebug ===> Se ha marcado una alerta como vista!");
    _list[idx].setAsChecked(userName);
    _alertsReference.document(_list[idx].id).updateData(_list[idx].toJson());
    notifyListeners();
  }

  void addComment(int idx, String comment) {
    print("MyDebug ==> Se ha añadido un comentario a una a alerta");
    _list[idx].addComment(comment);
    _alertsReference.document(_list[idx].id).updateData(_list[idx].toJson());
    notifyListeners();
  }

  void undoCheck(int idx){
    print("MyDebug ===> Deshaciendo selección!");
    _list[idx].undoCheck();
    _alertsReference.document(_list[idx].id).updateData(_list[idx].toJson());
    notifyListeners();
  }

  List<Alert> filterByCategory(String filterCategory){
    List<Alert> auxList;
    _list.forEach((alert) {
      if(alert.category==filterCategory){
        auxList.insert(0,alert);
      }
    });
    return auxList;
  }

  List<String> getListOfWorkers() {
    List<String> workers = List<String>();
    _usersReference = Firestore.instance.collection("Users");
    _usersReference.getDocuments().then((documents){
      documents.documents.forEach((document){
        workers.add(document.data['name']);
        print("I'm working!");
        });
    });
    return workers;
  }
}