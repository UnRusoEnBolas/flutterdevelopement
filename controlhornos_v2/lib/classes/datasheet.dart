import 'package:cloud_firestore/cloud_firestore.dart';

class DataSheet {
  String _id;
  String _whoUploaded;
  String _uploadDate;
  String _uploadTime;
  String _lastEditDate;
  String _lastEditTime;
  bool _isFinished;
  Map _data;
  bool _hasBeenRead;

  get id => _id;
  set id(id)=>_id=id;
  get whoUploaded => _whoUploaded;
  get uplaodDate => _uploadDate;
  get uploadTime => _uploadTime;
  get lastEditDate => _lastEditDate;
  get lastEditTime => _lastEditTime;
  get isFinished => _isFinished;
  get data => _data;
  get hasBeenRead => _hasBeenRead; //ACABAR AQUÍ

  DataSheet({String whoUploaded, String uploadDate, String uploadTime, bool isFinished, Map data}){
    _whoUploaded=whoUploaded;
    _uploadDate=uploadDate;
    _uploadTime=uploadTime;
    _isFinished=isFinished;
    _data=data;
    _hasBeenRead=false;
  }

  DataSheet.fromSnapshot(DocumentSnapshot s){
    _id = s.documentID;
    _whoUploaded = s.data['whoUploaded'];
    _uploadDate = s.data['uploadDate'];
    _uploadTime = s.data['uploadTime'];
    _lastEditDate = s.data['lastEditDate'];
    _lastEditTime = s.data['lastEditTime'];
    _isFinished = s.data['isFinished'];
    _data = s.data['data'];
    _hasBeenRead=s.data['hasBeenRead'];
  }

  editFromNewSnapshot(DocumentSnapshot s){
    _whoUploaded = s.data['whoUploaded'];
    _uploadDate = s.data['uploadDate'];
    _uploadTime = s.data['uploadTime'];
    _lastEditDate = s.data['lastEditDate'];
    _lastEditTime = s.data['lastEditTime'];
    _isFinished = s.data['isFinished'];
    _data = s.data['data'];
    _hasBeenRead=s.data['hasBeenRead'];
  }

  toJson(){
    return {
      "whoUploaded" : _whoUploaded,
      "uploadDate" : _uploadDate,
      "uploadTime" : _uploadTime,
      "lastEditDate" : _lastEditDate,
      "lastEditTime" : _lastEditTime,
      "isFinished" : _isFinished,
      "data" : _data,
      "hasBeenRead" : _hasBeenRead
    };
  }

  void setAsFinished(){
    _isFinished=true;
  }
}