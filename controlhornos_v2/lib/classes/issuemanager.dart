import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './issue.dart';

class IssueManager with ChangeNotifier{
  List<Issue> _list=[];
  CollectionReference _issuesReference;

  IssueManager(){
    _issuesReference = Firestore.instance.collection('Issues');
    _issuesReference.snapshots().listen((data){
      data.documentChanges.forEach((change){
        switch(change.type.index){
          case 0:
            print("MyDebug ===> Se ha añadido una Issue");
            _list.insert(0, Issue.fromSnapshot(change.document));
            break;
          case 1:
            print("MyDebug ===> Se ha modificado una Issue");
            int idx = _list.indexWhere((item)=>item.id==change.document.documentID);
            _list[idx].editFromNewSnapshot(change.document);
            break;
          case 2:
            print("MyDebug ===> Se ha eliminado una Issue");
            _list.removeWhere((item)=>item.id==change.document.documentID);
            break;
        }
        notifyListeners();
      });
    });
  }

  List<Issue> get list => _list;

  void add(Issue i){
    _issuesReference.add(i.toJson());
    notifyListeners();
  }

  void edit(Issue i, int idx){
    _issuesReference.document(_list[idx].id).updateData(i.toJson());
    notifyListeners();
  }

  void setAsFinishedOvens(int idx){
    _list[idx].checkFinishedOvens();
    _issuesReference.document(_list[idx].id).updateData(_list[idx].toJson());
    notifyListeners();
  }

  void setAsFinishedMaintenance(int idx){
    _list[idx].checkedFinishedMaintenance();
    _issuesReference.document(_list[idx].id).updateData(_list[idx].toJson());
    notifyListeners();
  }
  
  int nNotFinished() {
    return _list.where((issue) => issue.isCompletelyFinished==false).length;
  }
}
