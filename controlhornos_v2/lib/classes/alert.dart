import 'package:cloud_firestore/cloud_firestore.dart';

class Alert {
  String _title;
  String _body;
  String _id;
  String _category;
  String _publishedDate;
  String _publishedTime;
  bool _hasBeenChecked;
  String _whoChecked;
  String _checkedDate;
  String _checkedTime;
  String _comment;
  String _sortDate;

  get sortDate => _sortDate;
  set sortDate(String s) => _sortDate=s;

  get title => _title;
  get body => _body;
  get id => _id;
  get hasBeenChecked => _hasBeenChecked;
  get whoChecked => _whoChecked;
  get publishedDate => _publishedDate;
  get publishedTime => _publishedTime;
  get category => _category;
  get comment => _comment;

  Alert({String title, String body, String id, String category,
  String publishedDate, String publishedTime, bool hasBeenChecked,
  String whoChecked, String checkedDate, String checkedTime, String comment}){
    _title=title;
    _body=body;
    _id=id;
    _category=category;
    _publishedDate=publishedDate;
    _publishedTime=publishedTime;
    _hasBeenChecked=hasBeenChecked;
    _whoChecked=whoChecked;
    checkedDate=checkedDate;
    checkedTime=checkedTime;
    _comment = comment;
  }

  Alert.fromSnapshot(DocumentSnapshot s){
    _id = s.documentID;
    _title = s.data['title'];
    _body = s.data['body'];
    _category = s.data['category'];
    _publishedDate = s.data['publishedDate'];
    _publishedTime = s.data['publishedTime'];
    _hasBeenChecked = s.data['hasBeenChecked'];
    _whoChecked = s.data['whoChecked'];
    _checkedDate = s.data['checkedDate'];
    _checkedTime = s.data['checkedTime'];
    _comment = s.data['comment'];
  }

  editFromNewSnapshot(DocumentSnapshot s){
    _title = s.data['title'];
    _body = s.data['body'];
    _category = s.data['category'];
    _publishedDate = s.data['publishedDate'];
    _publishedTime = s.data['publishedTime'];
    _hasBeenChecked = s.data['hasBeenChecked'];
    _whoChecked = s.data['whoChecked'];
    _checkedDate = s.data['checkedDate'];
    _checkedTime = s.data['checkedTime'];
    _comment = s.data['comment'];
  }

  toJson() {
    return{
      "title" : _title,
      "body" : _body,
      "category" : _category,
      "publishedDate" : _publishedDate,
      "publishedTime" : _publishedTime,
      "hasBeenChecked" : _hasBeenChecked,
      "whoChecked" : _whoChecked,
      "checkedDate" : _checkedDate,
      "checkedTime" : _checkedTime,
      "comment" : _comment
    };
  }

  void setAsChecked(userName){
    DateTime rightNow = DateTime.now();
    _hasBeenChecked=true;
    _whoChecked=userName;
    _checkedDate="${rightNow.year}-${rightNow.month}-${rightNow.day}";
    _checkedTime="${DateTime.now().toIso8601String().substring(11,19)}";
  }

  void addComment(String comment) {
    _comment=comment;
  }
  
  void undoCheck(){
    _hasBeenChecked=false;
    _whoChecked="";
    _checkedDate="";
    _checkedTime="";
  }
}