import 'package:cloud_firestore/cloud_firestore.dart';

class Issue{
  String _id;

  String _notificationTime;
  String _attendTime;
  String _deliveryTime;
  String _finishTime;

  bool _checkedFinishedOvens;
  bool _checkedFinishedMaintenance;
  bool _isCompletelyFinished;

  String _issueType;
  String _issueSubType;

  String _reporterUser;
  String _attendantUser;

  String _state; // NotificationEmmited - Attending - Delivered - Finished

  set id(id) => _id = id;
  get id => _id;
  get notificationTime => _notificationTime;
  get attendTime => _attendTime;
  get deliberyTime => _deliveryTime;
  get finishTime => _finishTime;
  get checkedFinishedOvens => _checkedFinishedOvens;
  get checkedFinishedMaintenance => _checkedFinishedMaintenance;
  get isCompletelyFinished => _isCompletelyFinished;
  get issueType => _issueType;
  get issueSubType => _issueSubType;
  get reporterUser => _reporterUser;
  get attendantUser => _attendantUser;
  get state => _state;

  Issue(
    {String notificationTime, String attendTime, String deliveryTime,
    String finishTime, bool checkedFinishedOvens, bool checkedFinishedMaintenance,
    bool isCompleteyFinished, String issueType, String issueSubtype, String reporterUser,
    String attendantUser, String state}) {
    _notificationTime = notificationTime;
    _attendTime = attendTime;
    _deliveryTime = deliveryTime;
    _finishTime = finishTime;
    _checkedFinishedOvens = checkedFinishedOvens;
    _checkedFinishedMaintenance = checkedFinishedMaintenance;
    _isCompletelyFinished = isCompleteyFinished;
    _issueType = issueType;
    _issueSubType = issueSubtype;
    _reporterUser = reporterUser;
    _attendantUser = attendantUser;
    _state = state;
  }

  Issue.fromSnapshot(DocumentSnapshot s){
    var data = s.data;
    _id = s.documentID;
    _notificationTime = data['notificationTime'];
    _attendTime = data['attendTime'];
    _deliveryTime = data['deliveryTime'];
    _finishTime = data['finishTime'];
    _checkedFinishedOvens = data['checkedFinishedOvens'];
    _checkedFinishedMaintenance = data['chckedFinishedMaintenance'];
    _isCompletelyFinished = data['isCompletelyFinished'];
    _issueType = data['issueType'];
    _issueSubType = data['issueSubType'];
    _reporterUser = data['reporterUser'];
    _attendantUser = data['attendantUser'];
    _state = data['state'];
  }

  toJson(){
    return {
      "notificationTime" : _notificationTime,
      "attendTime" : _attendTime,
      "deliveryTime" : _deliveryTime,
      "finishTime" : _finishTime,
      "checkedFinishedOvens" : _checkedFinishedOvens,
      "checkedFinishedMaintenance" : _checkedFinishedMaintenance,
      "isCompletelyFinished" : _isCompletelyFinished,
      "issueType" : _issueType,
      "issueSubType" : _issueSubType,
      "reporterUser" : _reporterUser,
      "attendantUser" : _attendantUser,
      "state" : _state
    };
  }

  editFromNewSnapshot(DocumentSnapshot s){
    var data = s.data;
    _notificationTime = data['notificationTime'];
    _attendTime = data['attendTime'];
    _deliveryTime = data['deliveryTime'];
    _finishTime = data['finishTime'];
    _checkedFinishedOvens = data['checkedFinishedOvens'];
    _checkedFinishedMaintenance = data['chckedFinishedMaintenance'];
    _isCompletelyFinished = data['isCompletelyFinished'];
    _issueType = data['issueType'];
    _issueSubType = data['issueSubType'];
    _reporterUser = data['reporterUser'];
    _attendantUser = data['attendantUser'];
    _state = data['state'];
  }

  void mainteanceCheckAttend(){
    _attendTime = DateTime.now().toIso8601String().substring(11,19);
    _state = "Atendiendo";
    
  }

  void checkIfFinished(){
    if(_checkedFinishedMaintenance && _checkedFinishedOvens){
      _isCompletelyFinished = true;
      _finishTime = DateTime.now().toIso8601String().substring(11,19);
      _state = "Finalizado";
    }
  }

  void checkFinishedOvens(){
    _checkedFinishedOvens = true;
    checkIfFinished();
  }

  void checkFinishedMaintenance(){
    _checkedFinishedMaintenance = true;
    checkIfFinished();
  }
}