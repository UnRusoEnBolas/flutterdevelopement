import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:json_parsing/main.dart';

Future<Map> getJSON() async {
  const String APIurl =
      "https://raw.githubusercontent.com/Glennmen/PMSF/master/static/data/pokemon.json";
  http.Response response = await http.get(APIurl);
  return json.decode(response.body);
}

class JSONParsingHome extends StatelessWidget {
  List<pokemonItem> pokeList;
  JSONParsingHome(this.pokeList);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(backgroundColor: Colors.yellow[700], child: Icon(Icons.search),),
        appBar: AppBar(
            elevation: 8.0,
            centerTitle: true,
            title: Padding(
              padding: const EdgeInsets.all(3.0),
              child: Image.asset('assets/images/poke.png', fit: BoxFit.cover),
            ),
            backgroundColor: Colors.blue),
        backgroundColor: Colors.blue,
        body: ListView.builder(
          itemCount: this.pokeList.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 6.0, right: 6.0, top: 0.5, bottom: 0.5),
                  child: Card(
                    color: Colors.white,
                    elevation: 2.5,
                    child: ListTile(
                        title: Text(this.pokeList[index].getName(),
                            style: TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 20)),
                        subtitle: Text(this.pokeList[index].getNumTypes() == 1
                            ? this.pokeList[index].getTypes()[0]
                            : "${this.pokeList[index].getTypes()[0]} - ${this.pokeList[index].getTypes()[1]} "),
                        trailing: Text(this.pokeList[index].getID().toString()),
                        leading: Image.asset(
                              "assets/images/pokemon/${this.pokeList[index].getID()}.png")),
                  ),
                ),
              ],
            );
          },
        ));
  }
}
