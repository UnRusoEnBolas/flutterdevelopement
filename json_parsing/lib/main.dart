import "package:flutter/material.dart";
import 'home.dart';

void main() async {
  Map _data = await getJSON();
  List<pokemonItem> _pokemonItems = [];
  for(String i in _data.keys){
    List<String> types = [];
    int n=0;
    for(Map j in _data[i]["types"]){
      types.add(j["type"]);
      n=n+1;
    }
    _pokemonItems.add(pokemonItem(int.parse(i), _data[i]["name"], _data[i]["rarity"], n, types));
  }
  runApp(MaterialApp(
      title: "JSON Parsing",
      debugShowCheckedModeBanner: false,
      home: JSONParsingHome(_pokemonItems)));
}

class pokemonItem {
  int _ID;
  String _name;
  String _rarity;
  int _numTypes;
  List<String> _types;

  pokemonItem(this._ID,this._name,this._rarity, this._numTypes, this._types);

  int getID() => _ID;
  String getName() => _name;
  String getRarity() => _rarity;
  List<String> getTypes() => _types;
  int getNumTypes() => _numTypes;
}
