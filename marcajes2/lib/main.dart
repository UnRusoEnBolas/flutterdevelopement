import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';

main() => runApp(MaterialApp(
    theme: ThemeData(fontFamily: 'Ubuntu'),
    title: "Registro Jornada Laboral",
    debugShowCheckedModeBanner: false,
    home: Homepage()));

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  static final _codeController = TextEditingController();
  static final _focoTexto = FocusNode();
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  final entradaTexto = TextField(
    style: TextStyle(color: Colors.white),
    textAlign: TextAlign.center,
    focusNode: _focoTexto,
    cursorColor: Colors.blue,
    controller: _codeController,
    keyboardType: TextInputType.number,
    decoration: InputDecoration(
      labelStyle: TextStyle(color: Colors.white),
      hintStyle: TextStyle(color: Colors.white.withAlpha(125)),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25),
          borderSide: BorderSide(color: Colors.white)),
      labelText: "Número de empleado",
      hintText: "Ejemplo: 12345",
      suffixIcon: Icon(Icons.person, color: Colors.white),
      fillColor: Colors.white,
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25),
          borderSide: BorderSide(color: Colors.white)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25),
          borderSide: BorderSide(color: Colors.white)),
    ),
    onChanged: (val) {
      print("Code length: ${_codeController.text.length.toString()}");
      if (_codeController.text.length == 5) {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      }
    },
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Text("Registro Jornada Laboral",
            style: TextStyle(fontWeight: FontWeight.w800)),
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(
              child: Padding(
                padding: const EdgeInsets.all(11.0),
                child: Image.asset(
                  "images/tfnlogo.png",
                  color: Colors.white,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: entradaTexto,
            ),
            Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          "Tiempo de trabajo",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                        ),
                        Divider(
                          color: Colors.white,
                          indent: 50,
                          endIndent: 50,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            RaisedButton(
                              color: Colors.green,
                              onPressed: () => sendRegisterMaster("OFFICE_IN"),//sendData("TRABAJO_ON"),
                              child: Text("Empezar"),
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25)),
                            ),
                            Icon(Icons.work, color: Colors.white, size: 50),
                            RaisedButton(
                              color: Colors.red,
                              onPressed: () => sendRegisterMaster("OFFICE_OUT"),  //sendData("TRABAJO_OFF"),
                              child: Text("Finalizar"),
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25)),
                            )
                          ],
                        )
                      ],
                    ),
                    Container(
                      height: 40,
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          "Tiempo de comida",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                        ),
                        Divider(
                          color: Colors.white,
                          indent: 50,
                          endIndent: 50,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            RaisedButton(
                              color: Colors.green,
                              onPressed: () => sendRegisterMaster("REST_START"), //sendData("COMIDA_ON"),
                              child: Text("Empezar"),
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25)),
                            ),
                            Icon(Icons.fastfood, color: Colors.white, size: 50),
                            RaisedButton(
                              color: Colors.red,
                              onPressed: () => sendRegisterMaster("REST_END"), //sendData("COMIDA_OFF"),
                              child: Text("Finalizar"),
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25)),
                            )
                          ],
                        )
                      ],
                    )
                  ],
                )),
          ],
        ),
      ),
    );
  }

  Future<bool> sendRegister(String _state) async {
    CollectionReference _referenciaMarcajes = Firestore.instance.collection('Marcajes');
    CollectionReference _referenciasEmpleados = Firestore.instance.collection('Empleados');
    var _empleadosDoc = await _referenciasEmpleados.getDocuments();
    bool exists = false;
    _empleadosDoc.documents.forEach((empleado){
      if(empleado['code']==_codeController.text) exists = true;
    });
    if(exists){
      DateTime _actualDateTime = DateTime.now();
      _referenciaMarcajes.add({
      'codigo':_codeController.text,
      'leido':false,
      'fecha':_actualDateTime.toIso8601String().substring(0, 10),
      'hora':_actualDateTime.toIso8601String().substring(11,19),
      'estado':_state
      });
      _codeController.text="";
    }
    return exists;
  }

  void sendRegisterMaster(String _state) async {
    bool wasCorrect = await sendRegister(_state);
    if(wasCorrect){
      showDialog(
                      context: context,
                      builder: (context) {
                        Future.delayed(Duration(seconds: 2), () {
                          Navigator.of(context).pop(true);
                        });
                        return AlertDialog(
                          title: Center(child: Text('Marcaje correcto!')),
                          backgroundColor: Colors.green,
                          titleTextStyle: TextStyle(fontSize: 30, color: Colors.white),
                          content: Icon(Icons.check, size: 40, color: Colors.white,),
                        );
                      });
    } else {
      showDialog(
                      context: context,
                      builder: (context) {
                        Future.delayed(Duration(seconds: 2), () {
                          Navigator.of(context).pop(true);
                        });
                        return AlertDialog(
                          title: Center(child: Text('Marcaje incorrecto!')),
                          backgroundColor: Colors.red,
                          titleTextStyle: TextStyle(fontSize: 30, color: Colors.yellow),
                          content: Icon(Icons.error, size: 40, color: Colors.yellow),
                        );
                      });
    }
  }
}