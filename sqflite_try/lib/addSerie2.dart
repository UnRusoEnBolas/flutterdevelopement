import 'package:flutter/material.dart';
import './Classes/imageapi.dart';
import './Classes/serie.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart';

class APISerie {
  int _id;
  String _name;
  String _imageURL;
  int _episodes;
  String _synopsis;
  String _startDate;

  APISerie(this._name, this._episodes, this._imageURL, this._synopsis, this._startDate, this._id);

  get name => this._name;
  get imageUrl => this._imageURL;
  get numEpisodes => this._episodes;
  get synopsis => this._synopsis;
  get startDate => this._startDate;
  get id => this._id;
}

class addSeriePage2 extends StatefulWidget {
  @override
  _addSeriePage2State createState() => _addSeriePage2State();
  List<APISerie> _results;
}

class _addSeriePage2State extends State<addSeriePage2> {
  int _state; // 0 -> Initial state // 1 -> All good // -1 -> Error // 2 -> Loading...

  void _fetchSeries(String name) async {
    debugPrint("Fetching for serie: $name");
    setState(() {
      _state = 2;
    });
    Map apiResponse = await ImageAPI().getRequest(name);

    if (apiResponse['meta']['count'] == 0) {
      setState(() {
        _state = -1;
      });
      return;
    }

    List<APISerie> _aux = [];
    for (int i = 0; i < apiResponse["data"].length; i++) {
      String name = apiResponse['data'][i]['attributes']['canonicalTitle'];
      int episodes = apiResponse['data'][i]['attributes']['episodeCount'];
      String imgurl = apiResponse['data'][i]['attributes']['posterImage']['small'];
      String synapsis = apiResponse['data'][i]['attributes']['synopsis'];
      String startDate = apiResponse['data'][i]['attributes']['startDate'];
      int id = int.parse(apiResponse['data'][i]['id']);
      _aux.add(APISerie(name, episodes, imgurl, synapsis, startDate, id));
    }
    setState(() {
      widget._results = []..addAll(_aux);
      _state = 1;
    });
  }

  void _showDialog() {
    TextEditingController _controller = TextEditingController();
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Anime search",
              style: TextStyle(color: Colors.white, fontSize: 17),
            ),
            content: TextField(
              controller: _controller,
              autofocus: true,
              autocorrect: false,
              cursorColor: Color(0xFF6FFFE9),
              keyboardType: TextInputType.text,
              style: TextStyle(color: Color(0xFF6FFFE9)),
              decoration: InputDecoration(
                  enabledBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
                  focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.white))),
            ),
            backgroundColor: Color(0xFF0B132B),
            actions: <Widget>[
              FlatButton(
                  child: Text("Cancel", style: TextStyle(color: Colors.red, fontSize: 13)),
                  onPressed: () => Navigator.pop(context)),
              Divider(color: Colors.transparent),
              FlatButton(
                child: Text("Search", style: TextStyle(color: Color(0xFF6FFFE9), fontSize: 13)),
                onPressed: () {
                  _fetchSeries(_controller.text);
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  @override
  void initState() {
    super.initState();
    widget._results = [];
    _state = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF0B132B),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Color(0xFF1C2541),
          onPressed: () {
            _showDialog();
          },
          child: Icon(
            Icons.search,
            color: Color(0xFF6FFFE9),
          ),
        ),
        appBar: AppBar(
            backgroundColor: Color(0xFF1C2541),
            title: Text(
              "Discover",
              style: TextStyle(fontSize: 28, fontWeight: FontWeight.w600, color: Colors.white),
            )),
        body: Padding(
            padding: const EdgeInsets.all(5.0),
            child: _state == 1
                ? ListView.builder(
                    itemCount: widget._results == null ? 0 : widget._results.length,
                    itemBuilder: (context, index) {
                      return OwnCardWidget(widget._results[index], index);
                    },
                  )
                : _state == -1
                    ? Center(
                        child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text("Nothing was founnd or there was a network error!",
                            textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 17)),
                      ))
                    : _state == 0
                        ? Center(
                            child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("Write the name of the anime and hit the search button!",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white, fontSize: 17)),
                          ))
                        : Center(
                            child: SpinKitWave(
                              color: Color(0xFF6FFFE9),
                            ),
                          )));
  }
}

class OwnCardWidget extends StatelessWidget {
  APISerie _serie;
  int _index;
  OwnCardWidget(this._serie, this._index);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      elevation: 8.0,
      margin: EdgeInsets.symmetric(vertical: 6.0, horizontal: 10.0),
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Color(0xFF1C2541)),
        child: ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          leading: Container(
              padding: EdgeInsets.only(right: 20.0),
              decoration: BoxDecoration(border: Border(right: BorderSide(color: Colors.white24, width: 1.0))),
              child: CircleAvatar(
                backgroundImage: NetworkImage(_serie._imageURL),
                radius: 30,
              )),
          title: Text(
            _serie._name,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17),
          ),
          subtitle: Row(
            children: <Widget>[
              Text(
                "Episodes: ${_serie._episodes == null ? 'Yet Unknown' : _serie._episodes}",
                style: TextStyle(color: Colors.white, fontSize: 13),
              )
            ],
          ),
          trailing: InkWell(
            child: Icon(Icons.keyboard_arrow_right, color: Color(0xFF6FFFE9), size: 23),
            onTap: () {
              print(_serie._synopsis);
              showModalBottomSheet(
                  context: context,
                  builder: (BuildContext context) {
                    return OwnDetailBottomSheet(_serie);
                  });
            },
            splashColor: Colors.white,
            highlightColor: Colors.white,
          ),
          onTap: () {
            showModalBottomSheet(
                context: context,
                builder: (BuildContext context) {
                  return OwnDetailBottomSheet(_serie);
                });
          },
        ),
      ),
    );
  }
}

class OwnDetailBottomSheet extends StatefulWidget {
  APISerie _serie;

  OwnDetailBottomSheet(this._serie);

  @override
  _OwnDetailBottomSheetState createState() => _OwnDetailBottomSheetState();
}

class _OwnDetailBottomSheetState extends State<OwnDetailBottomSheet> {
  int _selectedRadio;

  void createAndSaveSerie(APISerie apiSerie, int state) async {
    DBSerie dbSerie = DBSerie(id: apiSerie.id, name: apiSerie.name, totalEpisodes: apiSerie.numEpisodes);
    var response = await get(apiSerie.imageUrl);
    dbSerie.imageBytes = response.bodyBytes;

    if (state == 1) {
      dbSerie.state = 1;
      dbSerie.actualEpisode = 0;
    } else if (state == 2) {
      dbSerie.state = 2;
      dbSerie.actualEpisode = apiSerie.numEpisodes;
    } else if (state == 3) {
      dbSerie.state = 3;
      dbSerie.actualEpisode = 0;
    }

    dbSerie.saveToDatabase();
  }

  @override
  void initState() {
    super.initState();
    _selectedRadio = 1;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.transparent,
        child: Container(
          height: 500,
          decoration: BoxDecoration(
              color: Color(0xFF0B132B),
              borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          child: Padding(
            padding: const EdgeInsets.only(top: 3, bottom: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(Icons.keyboard_arrow_down, color: Colors.white, size: 30),
                Divider(
                  color: Colors.transparent,
                  height: 5,
                ),
                CircleAvatar(
                    radius: 75,
                    backgroundColor: Colors.white54,
                    backgroundImage: NetworkImage(widget._serie.imageUrl)),
                Divider(height: 7, color: Colors.transparent),
                Text(widget._serie._name,
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17)),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Episodes: ${widget._serie._episodes == null ? 'Yet Unknown' : widget._serie.numEpisodes}",
                        style: TextStyle(color: Colors.white54, fontSize: 13),
                      ),
                      Text(
                        "Start date: ${widget._serie.startDate == null ? 'Unknown' : widget._serie.startDate}",
                        style: TextStyle(color: Colors.white54, fontSize: 13),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.transparent,
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: Text(
                    widget._serie._synopsis,
                    style: TextStyle(color: Colors.white, fontSize: 13),
                    locale: Locale.fromSubtags(languageCode: 'es'),
                    maxLines: 7,
                    textAlign: TextAlign.justify,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Divider(color: Colors.transparent),
                //FROM HERE
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        height: 30,
                        width: 98,
                        child: RaisedButton(
                          color: Color(0xFF1C2541),
                          child: Text("Watching",
                              style: TextStyle(
                                  color: _selectedRadio == 1 ? Color(0xFF6FFFE9) : Colors.white54,
                                  fontSize: 13)),
                          onPressed: () {
                            setState(() {
                              _selectedRadio = 1;
                            });
                          },
                        ),
                      ),
                      Container(
                        height: 30,
                        width: 98,
                        child: RaisedButton(
                          color: Color(0xFF1C2541),
                          child: Text("Completed",
                              style: TextStyle(
                                  color: _selectedRadio == 2 ? Color(0xFF6FFFE9) : Colors.white54,
                                  fontSize: 13)),
                          onPressed: () {
                            setState(() {
                              _selectedRadio = 2;
                            });
                          },
                        ),
                      ),
                      Container(
                        height: 30,
                        width: 98,
                        child: RaisedButton(
                          color: Color(0xFF1C2541),
                          child: Text("Pending",
                              style: TextStyle(
                                  color: _selectedRadio == 3 ? Color(0xFF6FFFE9) : Colors.white54,
                                  fontSize: 13)),
                          onPressed: () {
                            setState(() {
                              _selectedRadio = 3;
                            });
                          },
                        ),
                      ),
                      InkWell(
                        child: Container(
                            height: 42,
                            width: 42,
                            decoration: BoxDecoration(color: Color(0xFF1C2541), shape: BoxShape.circle),
                            child: Icon(
                              Icons.check,
                              color: Color(0xFF6FFFE9),
                            )),
                        onTap: () {
                          createAndSaveSerie(widget._serie, _selectedRadio);
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
  }
}
