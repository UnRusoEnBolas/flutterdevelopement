import 'package:flutter/material.dart';
import 'package:sqflite_try/addSerie2.dart';
import './Classes/dbhelper.dart';
import './Classes/serie.dart';
import 'dart:typed_data';
import './addSerie.dart';

void main() {
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'ProductSans', canvasColor: Colors.transparent),
      title: "SQFLite",
      home: _TabBarPage()));
}

class _TabBarPage extends StatefulWidget {
  @override
  __TabBarPageState createState() => __TabBarPageState();
}

class __TabBarPageState extends State<_TabBarPage> with SingleTickerProviderStateMixin {
  var _tabController;
  List<DBSerie> _watchingSeriesList;
  List<DBSerie> _completedSeriesList;
  List<DBSerie> _pendingSeriesList;
  DBManager _databaseManager;

  void initDatabase() async {
    _databaseManager = DBManager();
    await _databaseManager.startDB();
    refreshSeriesList();
  }

  void refreshSeriesList() async {
    _watchingSeriesList = await _databaseManager.getWatchingSeries();
    _completedSeriesList = await _databaseManager.getCompletedSeries();
    _pendingSeriesList = await _databaseManager.getPendingSeries();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    initDatabase();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Material(
            color: Color(0xFF1C2541), child: _ownDrawerContent(_databaseManager, refreshSeriesList, context)),
      ),
      appBar: AppBar(
        backgroundColor: Color(0xFF1C2541),
        title: Text(
          "Watchlist",
          style: TextStyle(fontSize: 28, fontWeight: FontWeight.w600),
        ),
      ),
      bottomNavigationBar: Material(
        color: Color(0xFF0B132B),
        child: Container(
          color: Colors.transparent,
          child: Container(
            decoration: BoxDecoration(
                color: Color(0xFF1C2541),
                borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10))),
            child: TabBar(
                unselectedLabelColor: Color(0xFF3A506B),
                labelColor: Color(0xFF6FFFE9),
                indicatorWeight: 5,
                indicatorColor: Color(0xFF6FFFE9),
                controller: _tabController,
                tabs: _ownTabBar),
          ),
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: <Widget>[
          Material(
              color: Color(0xFF0B132B),
              child: FutureBuilder<List<DBSerie>>(
                  future: _databaseManager.getWatchingSeries(),
                  builder: (BuildContext context, AsyncSnapshot<List<DBSerie>> snapshot) {
                    if (snapshot.hasData) {
                      return ListView.separated(
                        separatorBuilder: (context, index) {
                          return Divider(
                            color: Color(0xFF3A506B),
                          );
                        },
                        itemCount: _watchingSeriesList == null ? 0 : _watchingSeriesList.length,
                        itemBuilder: (context, index) {
                          return _ownListTile(
                              refreshSeriesList, _databaseManager, _watchingSeriesList[index], context);
                        },
                      );
                    } else {
                      return Center(
                          child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Color(0xFF6FFFE9)),
                      ));
                    }
                  })),
          Material(
              color: Color(0xFF0B132B),
              child: FutureBuilder<List<DBSerie>>(
                  future: _databaseManager.getCompletedSeries(),
                  builder: (BuildContext context, AsyncSnapshot<List<DBSerie>> snapshot) {
                    if (snapshot.hasData) {
                      return ListView.separated(
                        separatorBuilder: (context, index) {
                          return Divider(
                            color: Color(0xFF3A506B),
                          );
                        },
                        itemCount: _completedSeriesList == null ? 0 : _completedSeriesList.length,
                        itemBuilder: (context, index) {
                          return _ownListTile(
                              refreshSeriesList, _databaseManager, _completedSeriesList[index], context);
                        },
                      );
                    } else {
                      return Center(
                          child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Color(0xFF6FFFE9)),
                      ));
                    }
                  })),
          Material(
              color: Color(0xFF0B132B),
              child: FutureBuilder<List<DBSerie>>(
                  future: _databaseManager.getPendingSeries(),
                  builder: (BuildContext context, AsyncSnapshot<List<DBSerie>> snapshot) {
                    if (snapshot.hasData) {
                      return ListView.separated(
                        separatorBuilder: (context, index) {
                          return Divider(
                            color: Color(0xFF3A506B),
                          );
                        },
                        itemCount: _pendingSeriesList == null ? 0 : _pendingSeriesList.length,
                        itemBuilder: (context, index) {
                          return _ownListTile(
                              refreshSeriesList, _databaseManager, _pendingSeriesList[index], context);
                        },
                      );
                    } else {
                      return Center(
                          child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Color(0xFF6FFFE9)),
                      ));
                    }
                  })),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xFF1C2541),
        child: Icon(
          Icons.add,
          color: Color(0xFF6FFFE9),
        ),
        onPressed: () {
          Navigator.push(
                  context, //HERE
                  MaterialPageRoute(builder: (context) => /*addSeriePage(_databaseManager)*/ addSeriePage2()))
              .then((val) {
            refreshSeriesList();
          });
        },
      ),
    );
  }
}

//=======================================================================================================//
Widget _ownListTile(Function refreshList, DBManager databasemanagaer, DBSerie serie, BuildContext context) {
  return Container(
    height: 70,
    child: ListTile(
      title:
          Text(serie.name, style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.w800)),
      trailing:
          Text("${serie.actualEpisode}/${serie.totalEpisodes}", style: TextStyle(color: Colors.white54)),
      leading: Stack(
        fit: StackFit.passthrough,
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          CircleAvatar(
            radius: 30,
            backgroundImage:
                serie.imageBytes == null ? null : MemoryImage(Uint8List.fromList(serie.imageBytes)),
            backgroundColor: Colors.transparent,
          ),
          SizedBox(
            height: 60,
            width: 60,
            child: CircularProgressIndicator(
              value: serie.actualEpisode / serie.totalEpisodes,
              valueColor: AlwaysStoppedAnimation<Color>(Color(0xFF6FFFE9)),
              backgroundColor: Color(0xFF3A506B),
              strokeWidth: 3.5,
            ),
          )
        ],
      ),
      onLongPress: () {
        showModalBottomSheet(
            context: context,
            builder: (BuildContext context) {
              return Container(
                color: Colors.transparent,
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(0xFF1C2541),
                      borderRadius:
                          BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10))),
                  child: ListTile(
                    leading: Icon(Icons.delete_forever, color: Colors.red, size: 30),
                    title: Text(
                      "Delete serie",
                      style: TextStyle(color: Colors.red, fontSize: 20),
                    ),
                    onTap: () {
                      databasemanagaer.deleteSerieByID(serie.id);
                      refreshList();
                      Navigator.pop(context);
                    },
                  ),
                ),
              );
            });
      },
      onTap: () {
        showModalBottomSheet(
            context: context,
            builder: (BuildContext context) {
              return _ownDetailedSheet(serie, refreshList);
            });
      },
    ),
  );
}

//=======================================================================================================//
List<Widget> _ownTabBar = [
  Padding(
    padding: const EdgeInsets.all(6.0),
    child: Text(
      "Watching",
      style: TextStyle(fontSize: 17),
    ),
  ),
  Padding(
    padding: const EdgeInsets.all(6.0),
    child: Text(
      "Completed",
      style: TextStyle(fontSize: 17),
    ),
  ),
  Padding(
    padding: const EdgeInsets.all(6.0),
    child: Text(
      "Pending",
      style: TextStyle(fontSize: 17),
    ),
  ),
];

//=======================================================================================================//

Widget _ownDrawerContent(DBManager db, Function refresh, BuildContext context) {
  return Column(
    children: <Widget>[
      DrawerHeader(
        child: Container(color: Color(0xFF0B132B)),
      ),
      ListTile(
        leading: IconButton(
          icon: Icon(Icons.clear_all, color: Color(0xFF6FFFE9)),
          onPressed: () {
            db.truncateDatabase();
            refresh();
            Navigator.of(context).pop();
          },
        ),
        title: Text(
          "Clear database",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        subtitle: Text(
          "Caution, this will erase all the data abour your series!",
          style: TextStyle(color: Colors.white54, fontSize: 13),
        ),
      )
    ],
  );
}

//=======================================================================================================//

class _ownDetailedSheet extends StatefulWidget {
  DBSerie serie2Detail;
  Function refreshList;

  _ownDetailedSheet(this.serie2Detail, this.refreshList);

  @override
  __ownDetailedSheetState createState() => __ownDetailedSheetState();
}

class __ownDetailedSheetState extends State<_ownDetailedSheet> {

  void addEpisode() async {
    int actualEp = widget.serie2Detail.actualEpisode;
    int maxEp = widget.serie2Detail.totalEpisodes;
    if (actualEp++ <= maxEp) {
      setState(() {
        widget.serie2Detail.actualEpisode = widget.serie2Detail.actualEpisode++;
        //widget.refreshList();
      });
    }
  }

  void substractEpisode() {
    int actualEp = widget.serie2Detail.actualEpisode;
    if (actualEp-- > 0) {
      setState(() {
        widget.serie2Detail.alterActualEpisode(actualEp--);
        widget.refreshList();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Container(
        height: 370,
        decoration: BoxDecoration(
            color: Color(0xFF1C2541),
            borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15))),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Stack(
                alignment: AlignmentDirectional.center,
                fit: StackFit.passthrough,
                children: <Widget>[
                  CircleAvatar(
                    radius: 90,
                    backgroundColor: Colors.white54,
                    backgroundImage: widget.serie2Detail.imageBytes != null
                        ? MemoryImage(Uint8List.fromList(widget.serie2Detail.imageBytes))
                        : null,
                  ),
                  SizedBox(
                    height: 180,
                    width: 180,
                    child: CircularProgressIndicator(
                      value: widget.serie2Detail.actualEpisode/widget.serie2Detail.totalEpisodes,
                      valueColor: AlwaysStoppedAnimation<Color>(Color(0xFF6FFFE9)),
                      backgroundColor: Color(0xFF3A506B),
                      strokeWidth: 7,
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  widget.serie2Detail.name,
                  style: TextStyle(color: Colors.white, fontSize: 25, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {
                        substractEpisode();
                      },
                      child: Icon(
                        Icons.remove,
                        color: Color(0xFF6FFFE9),
                        size: 35,
                      ),
                    ),
                    RichText(
                        text: TextSpan(
                            text: "${widget.serie2Detail.actualEpisode.toString()}",
                            style: TextStyle(
                                color: Color(0xFF6FFFE9), fontSize: 50, fontWeight: FontWeight.bold),
                            children: [
                          TextSpan(text: "/", style: TextStyle(color: Colors.white54, fontSize: 30)),
                          TextSpan(
                              text: "${widget.serie2Detail.totalEpisodes.toString()}",
                              style: TextStyle(color: Colors.white54, fontSize: 30))
                        ])),
                    FlatButton(
                      onPressed: () {
                        addEpisode();
                      },
                      child: Icon(Icons.add, color: Color(0xFF6FFFE9), size: 35),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
