/*import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:sqflite_try/Classes/imageapi.dart';
import './Classes/dbhelper.dart';
import './Classes/serie.dart';
import 'package:image_picker/image_picker.dart';
import './Classes/imageapi.dart';

class addSeriePage extends StatefulWidget {
  DBManager dbManager;
  addSeriePage(this.dbManager);

  @override
  _addSeriePageState createState() => _addSeriePageState();
}

class _addSeriePageState extends State<addSeriePage> {
  final _formKey = GlobalKey<FormState>();
  Serie _serieCreada = Serie(null, '', '', 0, 0, '', null);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF1C2541),
          title: Text("Add a new serie",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w200)),
          leading: IconButton(
            icon: Icon(Icons.close, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        backgroundColor: Color(0xFF0B132B),
        floatingActionButton: FloatingActionButton(
            backgroundColor: Color(0xFF1C2541),
            child: Icon(Icons.check, color: Color(0xFF6FFFE9)),
            onPressed: () {
              _formKey.currentState.validate();
              _formKey.currentState.save();
              widget.dbManager.insertSerie(_serieCreada);
              Navigator.pop(context);
            }),
        body: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    height: 50,
                    width: 900,
                    color: Colors.transparent,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 12.0, 0, 0),
                      child: Text(
                        "Fill these fields to register a new serie:",
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                  ),
                  _ownImageSelectButton(_serieCreada),
                  _ownTextFormField(_serieCreada, "Serie Name",
                      _serieCreada.setSerieName, false),
                  _ownTextFormField(_serieCreada, "Watching Site",
                      _serieCreada.setWatchingSite, false),
                  _ownTextFormField(_serieCreada, "Actual episode",
                      _serieCreada.setActualCap, true),
                  _ownTextFormField(_serieCreada, "Total episodes",
                      _serieCreada.setTotalCaps, true),
                  _ownStateSelector(_serieCreada),
                ],
              ),
            )));
  }
}

class _ownTextFormField extends StatefulWidget {
  Serie _serie;
  String _textFieldLabel;
  Function _function;
  bool _isInt;

  _ownTextFormField(
      this._serie, this._textFieldLabel, this._function, this._isInt);

  @override
  __ownTextFormFieldState createState() => __ownTextFormFieldState();
}

class __ownTextFormFieldState extends State<_ownTextFormField> {
  FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 8, 20, 8),
      child: TextFormField(
        style: TextStyle(color: Colors.white),
        keyboardType: widget._isInt ? TextInputType.number : TextInputType.text,
        focusNode: _focusNode,
        validator: (val) {
          if (val.isEmpty) {
            return "Please fill this field";
          }
          if (widget._isInt && int.tryParse(val) == null) {
            return 'This value must be a number';
          }
          return null;
        },
        onSaved: (val) {
          setState(() {
            widget._isInt
                ? widget._function(int.parse(val))
                : widget._function(val);
          });
        },
        cursorColor: Color(0xFF6FFFE9),
        decoration: InputDecoration(
            labelText: widget._textFieldLabel,
            labelStyle: TextStyle(
                color:
                    _focusNode.hasFocus ? Color(0xFF6FFFE9) : Colors.white54),
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white54),
                borderRadius: BorderRadius.circular(8)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Color(0xFF6FFFE9),
                ),
                borderRadius: BorderRadius.circular(8)),
            errorBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.redAccent,
                ),
                borderRadius: BorderRadius.circular(8)),
            focusedErrorBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.redAccent,
                ),
                borderRadius: BorderRadius.circular(8))),
      ),
    );
  }
}

class _ownStateSelector extends StatefulWidget {
  Serie _serie;
  _ownStateSelector(this._serie);
  @override
  __ownStateSelectorState createState() => __ownStateSelectorState();
}

class __ownStateSelectorState extends State<_ownStateSelector> {
  String _stateValue;
  int _selectedRadio = 1;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget._serie.setState("Watching");
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              height: 35,
              width: 115,
              child: RaisedButton(
                color: Color(0xFF1C2541),
                child: Text("Watching",
                    style: TextStyle(
                        color: _selectedRadio == 1
                            ? Color(0xFF6FFFE9)
                            : Colors.white54,
                        fontSize: 16)),
                onPressed: () {
                  setState(() {
                    _selectedRadio = 1;
                    _stateValue = "Watching";
                    widget._serie.setState("Watching");
                  });
                },
              ),
            ),
            Container(
              height: 35,
              width: 115,
              child: RaisedButton(
                color: Color(0xFF1C2541),
                child: Text("Completed",
                    style: TextStyle(
                        color: _selectedRadio == 2
                            ? Color(0xFF6FFFE9)
                            : Colors.white54,
                        fontSize: 16)),
                onPressed: () {
                  setState(() {
                    _selectedRadio = 2;
                    _stateValue = "Completed";
                    widget._serie.setState("Completed");
                  });
                },
              ),
            ),
            Container(
              height: 35,
              width: 115,
              child: RaisedButton(
                color: Color(0xFF1C2541),
                child: Text("Pending",
                    style: TextStyle(
                        color: _selectedRadio == 3
                            ? Color(0xFF6FFFE9)
                            : Colors.white54,
                        fontSize: 16)),
                onPressed: () {
                  setState(() {
                    _selectedRadio = 3;
                    _stateValue = "Pending";
                    widget._serie.setState("Pending");
                  });
                },
              ),
            )
          ],
        ));
  }
}

class _ownImageSelectButton extends StatefulWidget {
  Serie _serie;
  _ownImageSelectButton(this._serie);

  @override
  __ownImageSelectButtonState createState() => __ownImageSelectButtonState();
}

class __ownImageSelectButtonState extends State<_ownImageSelectButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 8),
        child: Container(
          child: Stack(alignment: AlignmentDirectional.center, children: [
            GestureDetector(
              child: CircleAvatar(
                radius: 75,
                backgroundColor: Color(0xFF3A506B),
                backgroundImage: widget._serie.getSeriePic() == null
                    ? null
                    : MemoryImage(widget._serie.getSeriePic()),
              ),
              onTap: () async {
                //var img = await ImagePicker.pickImage(source: ImageSource.gallery);
                var img = await ImageAPI().getImageURL("Shingeki No Kyojin");

                var byteList;
                img != null ? byteList = img : debugPrint("No image selected!");
                setState(() {
                  widget._serie.setPic(byteList);
                });
              },
            ),
            widget._serie.getSeriePic() == null
                ? Text("Tap here to fetch an image",
                    style: TextStyle(color: Colors.white))
                : Text("")
          ]),
        ));
  }
}
*/
