import 'dart:io';
import 'dbhelper.dart';

/*class Serie {
  int _id; //ID of teh serie in the database
  int _totalCaps; //Total amount of chapters this serie has
  int _actualCap; //Actual chapter you are at
  String _serieName; //How's the serie named?
  String _serieState; //Watching - Completed - Pending
  List<int> _seriePic;

  Map<String, dynamic> toMap() {
    return {
      'SerieID': _id,
      'SerieName': _serieName,
      'SerieWatchingSite': _watchSite,
      'SerieActualEp': _actualCap,
      'SerieTotalEp': _totalCaps,
      'SerieState': _serieState,
      'SeriePic': _seriePic
    };
  }
}*/

class DBSerie {
  int _id;
  String _name;
  int _totalEpisodes;
  int _actualEpisode;
  List<int> _imageBytes;
  int _serieState;

  DBSerie({int id, String name, int totalEpisodes, int actualEpisode, int state, List<int> imageBytes}) {
    id != null ? this._id = id : this._id = null;
    name != null ? this._name = name : this._name = null;
    totalEpisodes != null ? this._totalEpisodes = totalEpisodes : this._totalEpisodes = null;
    actualEpisode != null ? this._actualEpisode = actualEpisode : this._actualEpisode = null;
    state != null ? this._serieState = state : this._serieState = null;
    imageBytes != null ? this._imageBytes = imageBytes : this._imageBytes = null;
  }

  get id => this._id;
  get name => this._name;
  get totalEpisodes => this._totalEpisodes;
  get actualEpisode => this._actualEpisode;
  get state => this._serieState;
  get imageBytes => this._imageBytes;

  set id(int id) => this._id = id;
  set name(String name) => this._name = name;
  set totalEpisodes(int totalEpisodes) => this._totalEpisodes = totalEpisodes;
  set imageBytes(List<int> imageBytes) => this._imageBytes = imageBytes;
  set state(int state) => this._serieState = state;
  set actualEpisode(int actualEpisode) => this._actualEpisode = actualEpisode;

  void alterState(int state) {
    this._serieState = state;
    DBManager().updateState(this._id, state);
  }

  void alterActualEpisode(int actualEpisode) {
    this._actualEpisode = actualEpisode;
    DBManager().updateEpisode(this._id, actualEpisode);
  }

  void saveToDatabase() {
    DBManager().saveSerie(this);
  }
}
