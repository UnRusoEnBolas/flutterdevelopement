import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import './serie.dart';

class DBManager {
  final String _dbName = "Serie_Database.db";
  final String _tableName = "SeriesTable";
  final String _idField = "SerieID";
  final String _nameField = "SerieName";
  final String _actualEpField = "SerieActualEp";
  final String _totalEpField = "SerieTotalEp";
  final String _stateField = 'SerieState';
  final String _pictureField = 'SeriePic';
  final String _primaryKey = 'PrimaryKey';
  Database db;

  DBManager() {
    if (db == null) {
      startDB();
    }
  }

  Future<void> startDB() async {
    db = await openDatabase(
      join(await getDatabasesPath(), _dbName),
      onCreate: (db, version) {
        return db.execute(
            "CREATE TABLE $_tableName($_primaryKey INTEGER PRIMARY KEY AUTOINCREMENT, $_idField INTEGER, $_nameField TEXT, $_actualEpField INTEGER, $_totalEpField INTEGER, $_stateField INTEGER, $_pictureField TEXT)");
      }, // ID -- NAME -- ACTUALEPISODE -- TOTALEPISODES -- STATE -- IMAGE //
      version: 1,
    );
  }

  Future<void> saveSerie(DBSerie serie) async {
    if (this.db == null) await startDB();
    print(serie.imageBytes);
    await db.rawQuery(
        "INSERT INTO $_tableName($_primaryKey, $_idField, $_nameField, $_actualEpField, $_totalEpField, $_stateField, $_pictureField) VALUES(NULL, ${serie.id}, '${serie.name}', ${serie.actualEpisode}, ${serie.totalEpisodes}, ${serie.state}, '${serie.imageBytes}')");
  }

  Future<void> truncateDatabase() async {
    db.delete(_tableName);
  }

  Future<void> deleteSerieByID(int serieID) async {
    await db.rawQuery("DELETE FROM $_tableName WHERE $_idField=$serieID");
  }

  Future<void> updateEpisode(int serieID, int episodeNum) async {
    await db.rawQuery("UPDATE $_tableName SET $_actualEpField = $episodeNum WHERE $_idField = $serieID");
  }

  Future<void> updateState(int serieID, int state) async {
    await db.rawQuery("UPDATE $_tableName SET $_stateField = $state WHERE $_idField = $serieID");
  }

  Future<List<DBSerie>> getSeries() async {
    final List<Map<String, dynamic>> maps = await db.query("$_tableName");

    return List.generate(maps.length, (i) {
      return DBSerie(
          id: maps[i]['$_idField'],
          name: maps[i]['$_nameField'],
          totalEpisodes: maps[i]['$_totalEpField'],
          actualEpisode: maps[i]['$_actualEpField'],
          state: maps[i]['$_stateField'],
          imageBytes: maps[i]['$_pictureField']);
    });
  }

  Future<List<DBSerie>> getWatchingSeries() async {
    final List<Map<String, dynamic>> maps =
        await db.rawQuery("SELECT * FROM $_tableName WHERE $_stateField = 1");

    List<int> imageBytes = [];

    return List.generate(maps.length, (i) {
      if (maps.length > 0) {
      var splitted = maps[i]['$_pictureField'].replaceAll('[', '').replaceAll(']', '').split(", ");

      for (String pixel in splitted) {
        imageBytes.add(int.parse(pixel));
      }
      }
      return DBSerie(
          id: maps[i]['$_idField'],
          name: maps[i]['$_nameField'],
          totalEpisodes: maps[i]['$_totalEpField'],
          actualEpisode: maps[i]['$_actualEpField'],
          state: maps[i]['$_stateField'],
          imageBytes: imageBytes);
    });
  }

  Future<List<DBSerie>> getCompletedSeries() async {
    final List<Map<String, dynamic>> maps =
        await db.rawQuery("SELECT * FROM $_tableName WHERE $_stateField = 2");

    List<int> imageBytes = [];

    return List.generate(maps.length, (i) {
      if (maps.length > 0) {
      var splitted = maps[i]['$_pictureField'].replaceAll('[', '').replaceAll(']', '').split(", ");

      for (String pixel in splitted) {
        imageBytes.add(int.parse(pixel));
      }
    }
      return DBSerie(
          id: maps[i]['$_idField'],
          name: maps[i]['$_nameField'],
          totalEpisodes: maps[i]['$_totalEpField'],
          actualEpisode: maps[i]['$_actualEpField'],
          state: maps[i]['$_stateField'],
          imageBytes: imageBytes);
    });
  }

  Future<List<DBSerie>> getPendingSeries() async {
    final List<Map<String, dynamic>> maps =
        await db.rawQuery("SELECT * FROM $_tableName WHERE $_stateField = 3");

    List<int> imageBytes = [];

    

    return List.generate(maps.length, (i) {
      if (maps.length > 0) {
      var splitted = maps[i]['$_pictureField'].replaceAll('[', '').replaceAll(']', '').split(", ");

      for (String pixel in splitted) {
        imageBytes.add(int.parse(pixel));
      }
    }
      return DBSerie(
          id: maps[i]['$_idField'],
          name: maps[i]['$_nameField'],
          totalEpisodes: maps[i]['$_totalEpField'],
          actualEpisode: maps[i]['$_actualEpField'],
          state: maps[i]['$_stateField'],
          imageBytes: imageBytes);
    });
  }
}
