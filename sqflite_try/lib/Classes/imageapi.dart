import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class ImageAPI {
  final _headers = {"Accept": "application/vnd.api+json", "Content-Type": "application/vnd.api+json"};

  Future<Map<String, dynamic>> getRequest(String name) async {
    String serieName = name.replaceAll(' ', '%20').toLowerCase();
    var response =
        await http.get("https://kitsu.io/api/edge/anime?filter[text]=$serieName", headers: _headers);
    var responseJson = json.decode(utf8.decode(response.bodyBytes));
    return responseJson;
  }
}
