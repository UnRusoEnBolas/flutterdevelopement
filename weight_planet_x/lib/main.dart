import 'package:flutter/material.dart';
import './UI/PlanetXHome.dart';

void main() => runApp(
  MaterialApp(
    title: "Weight on planet X",
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      primaryColor: Colors.white,
      appBarTheme: AppBarTheme(
        elevation: 0,
        color: Colors.blue[900],
      ),
      errorColor: Colors.red[900],
      highlightColor: Colors.deepPurple,
      accentColor: Colors.white,
      scaffoldBackgroundColor: Colors.blue[900],
      hintColor: Colors.white,
      unselectedWidgetColor: Colors.white,
    ),
    home: PlanetXHome()
  )
);