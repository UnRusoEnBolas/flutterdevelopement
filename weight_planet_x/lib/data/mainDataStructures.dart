import 'package:flutter/material.dart';

class Planet {
  String name = "";
  double gAcc = 0.0;
  int positionToSun = -1;

  Planet(this.name, this.positionToSun, this.gAcc);

  double weightOnPlanet(double mass) {
    return (mass * this.gAcc);
  }
}

