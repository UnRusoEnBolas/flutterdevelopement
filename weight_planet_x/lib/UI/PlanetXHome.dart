import 'package:flutter/material.dart';
import '../data/mainDataStructures.dart';

List<Planet> planetsArray = [
  new Planet("Mercury", 0, 3.59),
  new Planet("Venus", 1, 8.87),
  new Planet("The Earth", 2, 9.81),
  new Planet("Mars", 3, 3.77),
  new Planet("Jupiter", 4, 25.95),
  new Planet("Saturn", 5, 11.08),
  new Planet("Uranus", 6, 10.67),
  new Planet("Neptune", 7, 14.07),
  new Planet("Pluto", 8, 0.42)
];

class PlanetXHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("Weight on planet X",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25,
                    color: Colors.white)),
            centerTitle: true),
        body: Container(child: AppBody()));
  }
}

class AppBody extends StatefulWidget {
  @override
  _AppBodyState createState() => _AppBodyState();
}

class _AppBodyState extends State<AppBody> {
  int _selectedRadio = 0;
  double _userMass = 0;
  final TextEditingController _massInput = TextEditingController();

  void handleRadioChange(int val){
    setState(() {
      _selectedRadio=val;
      _userMass = planetsArray[_selectedRadio].weightOnPlanet(double.parse(_massInput.text));
    });
  }

  void handleTextChange(val){
    setState(() {
      if(!(val.isEmpty || double.parse(val)<0)){
        handleRadioChange(_selectedRadio);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Center(
                child: Image.asset("images/planet.png", width: 250, height: 200)),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: TextField(
                controller: _massInput,
                onChanged: handleTextChange,
                keyboardType: TextInputType.numberWithOptions(decimal: true),
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                decoration: InputDecoration(
                  labelText: "Enter your mass",
                  suffixText: "kg",
                  prefixIcon: Icon(Icons.person, color: Colors.white, size: 23),
                  border: OutlineInputBorder(
                      gapPadding: 10, borderRadius: BorderRadius.circular(8)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left:25.0, right:25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text("${planetsArray[0].name} >>", style: TextStyle(color:Colors.white),),
                  Radio<int>(
                    onChanged: handleRadioChange,
                    value:0,
                    groupValue: _selectedRadio,
                    activeColor: Colors.deepPurpleAccent,
                  ),
                  Text("${planetsArray[1].name} >>", style: TextStyle(color:Colors.white),),
                   Radio<int>(
                    onChanged: handleRadioChange,
                    value:1,
                    groupValue: _selectedRadio,
                    activeColor: Colors.deepPurpleAccent,
                  ),
                  Text("${planetsArray[2].name} >>", style: TextStyle(color:Colors.white),),
                  Radio<int>(
                    onChanged: handleRadioChange,
                    value:2,
                    groupValue: _selectedRadio,
                    activeColor: Colors.deepPurpleAccent,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left:25.0, right:25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text("${planetsArray[3].name} >>", style: TextStyle(color:Colors.white),),
                  Radio<int>(
                    onChanged: handleRadioChange,
                    value:3,
                    groupValue: _selectedRadio,
                    activeColor: Colors.deepPurpleAccent,
                  ),
                  Text("${planetsArray[4].name} >>", style: TextStyle(color:Colors.white),),
                   Radio<int>(
                    onChanged: handleRadioChange,
                    value:4,
                    groupValue: _selectedRadio,
                    activeColor: Colors.deepPurpleAccent,
                  ),
                  Text("${planetsArray[5].name} >>", style: TextStyle(color:Colors.white),),
                  Radio<int>(
                    onChanged: handleRadioChange,
                    value:5,
                    groupValue: _selectedRadio,
                    activeColor: Colors.deepPurpleAccent,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left:25.0, right:25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text("${planetsArray[6].name} >>", style: TextStyle(color:Colors.white),),
                  Radio<int>(
                    onChanged: handleRadioChange,
                    value:6,
                    groupValue: _selectedRadio,
                    activeColor: Colors.deepPurpleAccent,
                  ),
                  Text("${planetsArray[7].name} >>", style: TextStyle(color:Colors.white),),
                   Radio<int>(
                    onChanged: handleRadioChange,
                    value:7,
                    groupValue: _selectedRadio,
                    activeColor: Colors.deepPurpleAccent,
                  ),
                  Text("${planetsArray[8].name} >>", style: TextStyle(color:Colors.white),),
                  Radio<int>(
                    onChanged: handleRadioChange,
                    value:8,
                    groupValue: _selectedRadio,
                    activeColor: Colors.deepPurpleAccent,
                  ),
                ],
              ),
            ),
            Padding(padding: EdgeInsets.only(top:30),),
            Text("Your weight is", style: TextStyle(color: Colors.white, fontSize: 30)),
            Text("${_userMass.toStringAsFixed(2)}", style: TextStyle(color: Colors.deepPurpleAccent, fontWeight: FontWeight.w900, fontSize: 90)),
            Text("in ${planetsArray[_selectedRadio].name}", style: TextStyle(color: Colors.white, fontSize: 30))
          ]),
    );
  }
}
