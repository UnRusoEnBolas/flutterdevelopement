import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'misc.dart' as misc;

main() => runApp(MaterialApp(
    theme: ThemeData(fontFamily: 'Ubuntu'),
    title: "Registro Jornada Laboral",
    debugShowCheckedModeBanner: false,
    home: Homepage()));

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  static final _codeController = TextEditingController();
  static final _focoTexto = FocusNode();
  var _scaffoldKey = GlobalKey<ScaffoldState>();
  String _displayText =
      "Introduce tu código de empleado y usa el botón de enviar para fichar";
  String _displayIcon = "images/waiting.png";
  final entradaTexto = TextField(
    style: TextStyle(color: Colors.white),
    textAlign: TextAlign.center,
    focusNode: _focoTexto,
    cursorColor: Colors.blue,
    controller: _codeController,
    keyboardType: TextInputType.number,
    decoration: InputDecoration(
      labelStyle: TextStyle(color: Colors.white),
      hintStyle: TextStyle(color: Colors.white.withAlpha(125)),
      enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25),
          borderSide: BorderSide(color: Colors.white)),
      labelText: "Número de empleado",
      hintText: "Ejemplo: 12345",
      suffixIcon: Icon(Icons.person, color: Colors.white),
      fillColor: Colors.blue,
      border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25),
          borderSide: BorderSide(color: Colors.white)),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25),
          borderSide: BorderSide(color: Colors.white)),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: Text("Registro Jornada Laboral",
            style: TextStyle(fontWeight: FontWeight.w800)),
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Center(
              child: Padding(
                padding: const EdgeInsets.all(11.0),
                child: Image.asset(
                    "images/tfnlogo.png",
                    color: Colors.white,
                  ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: entradaTexto,
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Text(_displayText,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 27.0)),
            ),
            Padding(
              padding: const EdgeInsets.all(40.0),
              child: Image.asset(_displayIcon, color: Colors.white, height: 55),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          icon: Icon(
            Icons.send,
            color: Colors.black,
          ),
          label: Text(
            "Enviar",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w800),
          ),
          onPressed: () =>
              checkAndSendorDismiss(_codeController, context, _scaffoldKey),
          backgroundColor: Colors.white),
    );
  }

  void checkAndSendorDismiss(TextEditingController codeController,
      BuildContext context, GlobalKey<ScaffoldState> scaffoldKey) {
    FocusScope.of(context).requestFocus(new FocusNode());
    String inputedCode = codeController.text;
    codeController.text = "";
    if (checkCode(inputedCode)) {
      setState(() {
        _displayText = "Has fichado correctamente";
        _displayIcon = 'images/ok.png';
      });
      DateTime ahora = DateTime.now();
      String registroFinal = getRegistro(inputedCode);
      misc.postRegister(
          registroFinal, ahora.month.toString(), ahora.year.toString());
      mostrarSnack(inputedCode, ahora, context, scaffoldKey);
      Future.delayed(const Duration(seconds: 5), () {
        setState(() {
          _displayText =
              "Introduce tu código de empleado y usa el botón de enviar para fichar";
          _displayIcon = 'images/waiting.png';
          print("Time exceeded");
        });
      });
    } else {
      setState(() {
        _displayText = "Tu código no era correcto, prueba de nuevo";
        _displayIcon = 'images/notok.png';
      });
    }
  }
}

String getRegistro(String code) {
  //FORMATO: P010051yyyymmddhhmmssyyyymmddhhmmss000codig00
  var fechaActual = DateTime.now();
  String reg = "P010051";
  for (int i = 0; i < 2; i++) {
    reg += fechaActual.year.toString();
    reg += fechaActual.month.toString().length > 1
        ? fechaActual.month.toString()
        : "0${fechaActual.month.toString()}";
    reg += fechaActual.day.toString().length > 1
        ? fechaActual.day.toString()
        : "0${fechaActual.day.toString()}";
    reg += fechaActual.hour.toString().length > 1
        ? fechaActual.hour.toString()
        : "0${fechaActual.hour.toString()}";
    reg += fechaActual.minute.toString().length > 1
        ? fechaActual.minute.toString()
        : "0${fechaActual.minute.toString()}";
    reg += fechaActual.second.toString().length > 1
        ? fechaActual.second.toString()
        : "0${fechaActual.second.toString()}";
  }
  reg += "000";
  reg += code;
  reg += "00";
  reg += "\n";

  return reg;
}

bool checkCode(String code) {
  if (misc.codeDict.containsKey(code)) {
    debugPrint(misc.codeDict[code]);
    return true;
  }
  debugPrint("Code is NOT OK!");
  return false;
}

void mostrarSnack(String code, DateTime ahora, BuildContext context,
    GlobalKey<ScaffoldState> scaKey) {
  String hora;
  String minuto;

  ahora.hour.toString().length > 1
      ? hora = ahora.hour.toString()
      : hora = "0${ahora.hour.toString()}";
  ahora.minute.toString().length > 1
      ? minuto = ahora.minute.toString()
      : minuto = "0${ahora.minute.toString()}";

  String horaReg = "$hora:$minuto";

  SnackBar snack = SnackBar(
    content: Text(
      "Hola ${misc.codeDict[code]}, has fichado a las $horaReg.",
      style: TextStyle(color: Colors.black),
    ),
    duration: Duration(seconds: 4, milliseconds: 500),
    backgroundColor: Colors.white,
  );

  scaKey.currentState.showSnackBar(snack);
}

void hackAndSendOrDismiss(TextEditingController codeController,
    BuildContext context, GlobalKey<ScaffoldState> scaffoldKey) async {
  if (checkCode(codeController.text)) {
    var _horaElegida = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour: 0, minute: 00),
    );
    if (_horaElegida != null) {
      String horaElegida = aDosDigitos(_horaElegida.hour.toString());
      String minutoElegido = aDosDigitos(_horaElegida.minute.toString());
      print("$horaElegida:$minutoElegido");
      String registroHack =
          hackGetRegistro(codeController.text, horaElegida, minutoElegido);
      var snack = SnackBar(
        content: Text(
          "Hola ${misc.codeDict[codeController.text]}, has fichado a las $horaElegida:$minutoElegido.",
          style: TextStyle(color: Colors.black),
        ),
        duration: Duration(seconds: 4, milliseconds: 500),
        backgroundColor: Colors.white,
      );
      misc.postRegister(registroHack, DateTime.now().month.toString(), DateTime.now().year.toString());
      scaffoldKey.currentState.showSnackBar(snack);
    }
  }
}

String aDosDigitos(String numIni) {
  return numIni.length > 1 ? numIni : "0$numIni";
}

String hackGetRegistro(String code, String hora, String minuto) {
  //FORMATO: P010051yyyymmddhhmmssyyyymmddhhmmss000codig00
  var fechaActual = DateTime.now();
  String reg = "P010051";
  for (int i = 0; i < 2; i++) {
    reg += fechaActual.year.toString();
    reg += fechaActual.month.toString().length > 1
        ? fechaActual.month.toString()
        : "0${fechaActual.month.toString()}";
    reg += fechaActual.day.toString().length > 1
        ? fechaActual.day.toString()
        : "0${fechaActual.day.toString()}";
    reg += hora;
    reg += minuto;
    reg += fechaActual.second.toString().length > 1
        ? fechaActual.second.toString()
        : "0${fechaActual.second.toString()}";
  }
  reg += "000";
  reg += code;
  reg += "00";
  reg += "\n";

  return reg;
}
