import 'package:flutter/material.dart';
import './ui/homepage.dart';

main() => runApp(
  MaterialApp(
    title: "NodeControl",
    debugShowCheckedModeBanner: false,
    home: ControlHome()
  )
);