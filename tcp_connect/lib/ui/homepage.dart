import 'package:flutter/material.dart';
import '../misc.dart' as misc;

class ControlHome extends StatefulWidget {
  @override
  _ControlHomeState createState() => _ControlHomeState();
}

class _ControlHomeState extends State<ControlHome> {
  bool textFieldsAreReady(GlobalKey<ScaffoldState> k) {
    if (misc.txtIPcontroller.text.isEmpty ||
        misc.txtPortCotroller.text.isEmpty) {
      k.currentState.showSnackBar(SnackBar(
        content: Text("Please enter values to connect!"),
        duration: Duration(seconds: 3),
        action: SnackBarAction(
          textColor: Colors.blue,
          label: "Set default values",
          onPressed: setDefaultValues,
        ),
      ));
      return false;
    } else {
      misc.TCPConnexion(misc.txtIPcontroller.text, misc.txtPortCotroller.text);
      return true;
    }
  }

  void setDefaultValues() {
    misc.txtIPcontroller.text = misc.defaultIP;
    misc.txtPortCotroller.text = misc.defaultPort;
  }

  @override
  Widget build(BuildContext context) {
    final key = GlobalKey<ScaffoldState>();
    return Scaffold(
        key: key,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.blue,
          title: Text("NodeMCU Controller"),
        ),
        floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.blue,
            onPressed: () => print(textFieldsAreReady(key)),
            child: Icon(Icons.check)),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Center(
                  child: Image.asset("./images/smartphone.png", height: 200)),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Center(
                child: TextField(
                  controller: misc.txtIPcontroller,
                  keyboardType: TextInputType.number,
                  cursorColor: Colors.blue,
                  decoration: InputDecoration(
                      hintText: "Enter your NodeMCU's IP",
                      suffixIcon: Icon(Icons.alternate_email)),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Center(
                child: TextField(
                  controller: misc.txtPortCotroller,
                  keyboardType: TextInputType.number,
                  cursorColor: Colors.blue,
                  decoration: InputDecoration(
                      hintText: "Enter your NodeMCU's port",
                      suffixIcon: Icon(Icons.airplay)),
                ),
              ),
            )
          ],
        ));
  }
}
