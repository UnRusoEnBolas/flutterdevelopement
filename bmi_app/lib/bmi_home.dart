import 'package:flutter/material.dart';


class BMIHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        centerTitle: true,
        title: Text("BMI", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
        backgroundColor: Colors.teal,
      ),
      body: ListView(
        children: <Widget>[
          Image.asset('images/bmi_image.png', height: 250),
          InputBMIWidgets()
        ],
      )
    );
  }
}

class InputBMIWidgets extends StatefulWidget {
  @override
  _InputBMIWidgetsState createState() => _InputBMIWidgetsState();
}

class _InputBMIWidgetsState extends State<InputBMIWidgets> {
  double _userAge;
  double _userHeight;
  double _userWeight;
  String _userBMI = "";
  bool _buttonDisabled = true;
  final _ageTextController = TextEditingController();
  final _heightTextController = TextEditingController();
  final _weightTextController = TextEditingController();

  @override
  void initState(){
    super.initState();
    _ageTextController.addListener(handleAgeChange);
    _heightTextController.addListener(handleHeightChange);
    _weightTextController.addListener(handleWeightChange);
  }

  void checkIfReady(){
    if (_userAge != null && _userAge > 0 &&
    _userHeight != null && _userHeight > 0 &&
    _userWeight != null && _userWeight > 0){
      _buttonDisabled = false;
    } else {
      _buttonDisabled = true;
      _userBMI = "";
    }
  }
  void handleAgeChange(){
    setState((){
      if(_ageTextController.text.isEmpty)
        _userAge = null;
      else
        _userAge = double.parse(_ageTextController.text);
      debugPrint("NEW WEIGHT:  $_userAge");
      checkIfReady();
    });
  }
  void handleHeightChange(){
    if(_heightTextController.text.isEmpty)
        _userHeight = null;
      else
        _userHeight = double.parse(_heightTextController.text);
      debugPrint("NEW WEIGHT:  $_userHeight");
      checkIfReady();
  }
  void handleWeightChange(){
    setState((){
      if(_weightTextController.text.isEmpty)
        _userWeight = null;
      else
        _userWeight = double.parse(_weightTextController.text);
      debugPrint("NEW WEIGHT:  $_userWeight");
      checkIfReady();
    });
  }
  void calculateBMI(){
    setState((){
      _userBMI = (_userWeight/(_userHeight/100*_userHeight/100)).toStringAsFixed(2);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        _userBMI==null ? Container(height:1, width: 1) : Text(_userBMI, style: TextStyle(color:Colors.teal, fontWeight: FontWeight.bold, fontSize: 50)),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: TextField(
            controller: _ageTextController,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(suffixText:"years", border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)), labelText: "Your age"),
            ),
        ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: TextField(
              controller: _heightTextController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(suffixText: "cm", border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)), labelText: "Your height"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: TextField(
              controller: _weightTextController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(suffixText: "kg", border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)), labelText: "Your weight"),
            ),
          ),
          IconButton(
            onPressed: _buttonDisabled ? null : calculateBMI, 
            icon: Icon(Icons.refresh),
            iconSize: 70.0,
            disabledColor: Colors.grey,
            color: Colors.teal
          )
      ],
    );
  }
}