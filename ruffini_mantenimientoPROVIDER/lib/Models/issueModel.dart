import 'package:cloud_firestore/cloud_firestore.dart';
import '../Services/firebaseService.dart';

class IssueModel{
  String _notificationDate;
  String _notificationTime;
  String _issueType;
  String _issueSubType;
  bool _isCompletelyFinished;
  String _reporterUser;
  String _state;
  String _attendDate;
  String _attendTime;
  String _attendantUser;
  bool _checkedFinishedMaintenance;
  bool _checkedFinishedOvens;
  String _deliverydate;
  String _deliveryTime;
  String _finishDate;
  String _finishTime;
  String _id;

  FirebaseService firebaseAPI;

  String get id => _id;
  String get reporterUser => _reporterUser;
  String get notificationDate => _notificationDate;
  String get notificationTime => _notificationTime;
  String get issueType => _issueType;
  String get issueSubType => _issueSubType;
  String get state => _state;

  IssueModel.fromDocumentSnapshot(DocumentSnapshot snapShot){
    _id = snapShot.documentID;
    _reporterUser = snapShot.data['reporterUser'];
    _notificationDate = snapShot.data['notificationDate'];
    _notificationTime = snapShot.data['notificationTime'];
    _issueType = snapShot.data['issueType'];
    _issueSubType = snapShot.data['issueSubType'];
    _state = snapShot.data['state'];
    firebaseAPI = FirebaseService();
  }

  void assign(){
    firebaseAPI.setIssueAsAttending(_id);
  }

}