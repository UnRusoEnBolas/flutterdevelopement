import 'package:flutter/material.dart';
import '../Models/issueModel.dart';

class IssueListTile extends StatelessWidget {

  IssueModel _issue;

  IssueListTile({@required IssueModel issue}) {
    _issue = issue;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
          child: ExpansionTile(
        initiallyExpanded: false,
        title: Text(_issue.issueType, style: TextStyle(fontWeight: FontWeight.bold),),
        subtitle: Row(children: <Widget>[
          Text("Notificada el dia "),
          Text(_issue.notificationDate, style: TextStyle(fontWeight: FontWeight.bold),),
          Text(" a las "),
          Text(_issue.notificationTime, style: TextStyle(fontWeight: FontWeight.bold),)
        ],),
            children: <Widget>[
              Column(children: <Widget>[
                expansionChild(context, "Detalle de la avería: ", _issue.issueSubType),
                Divider(),
                expansionChild(context, "Se notificó por el usuario: ", _issue.reporterUser),
                Divider(),
                expansionChild(context, "Estado de la avería: ", _issue.state),
                Divider(),
                assignButton()
              ],)
            ],
          ),
      );
  }

  Widget expansionChild(BuildContext context, String field, String value){
    return RichText(
                  text: TextSpan(
                    text: field,
                    style: TextStyle(color: Colors.grey),
                    children: [
                      TextSpan(text:value, style: TextStyle(color:Colors.blue, fontWeight: FontWeight.bold))
                    ]
                  ),
                );
  }

  Widget assignButton() {
    return RaisedButton(
      color: Colors.blue,
      child: Text("Asignar a mi!", style: TextStyle(color: Colors.white),),
      onPressed: (){
       _issue.assign();
      },
    );
  }
}