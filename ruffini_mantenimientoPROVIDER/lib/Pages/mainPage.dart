import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../Models/issueModel.dart';
import '../Managers/issuesManager.dart';
import '../UIViews/issueListTile.dart';

class MainPage extends StatelessWidget {

  IssuesManager issuesManager;
  GlobalKey<AnimatedListState> _listKey;

  @override
  Widget build(BuildContext context) {

    issuesManager = Provider.of<IssuesManager>(context);
    _listKey=GlobalKey();

    return Scaffold(
      // body: ListView.builder(
      //         itemCount: issuesManager.notifiedIssuesList.length,
      //         itemBuilder: (_, idx) {
      //           IssueModel issue = issuesManager.notifiedIssuesList[idx];
      //           return IssueListTile(issue: issue);
      //         },
      //       )
      body: AnimatedList(
        key: _listKey,
        initialItemCount: issuesManager.notifiedIssuesList.length,
        itemBuilder: (context, idx, animation) => _buildItem(context, issuesManager.notifiedIssuesList[idx], animation),
      )
    );
  }

  Widget _buildItem(BuildContext context, IssueModel issueModel, Animation animation) {
    return SizeTransition(
      sizeFactor: animation,
      axis: Axis.vertical,
      child: IssueListTile(issue: issueModel,),
    );
  }
}