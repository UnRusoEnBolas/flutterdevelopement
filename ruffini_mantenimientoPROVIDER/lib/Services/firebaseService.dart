import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseService{
  Stream<QuerySnapshot> getNotifiedIssuesStream() {
    return Firestore.instance.collection("Issues").where('state', isEqualTo: 'Notificada').snapshots();
  }

  Stream<QuerySnapshot> getAttendingIssuesStream() {
    return Firestore.instance.collection("Issues").where('state', isEqualTo: 'Atendiendo').snapshots();
  }

  

  void setIssueAsAttending(String issueID){

    List<String> actualTime = _getActualTime();

    Firestore.instance.document('Issues/$issueID').updateData({
        "state" : "Atendiendo",
        "attendDate" : actualTime[0],
        "attendTime" : actualTime[1]
      }
    );
  }

  List<String> _getActualTime(){
    DateTime rightNow = DateTime.now();
    String date = "${rightNow.year}-${rightNow.month.toString().padLeft(2,'0')}-${rightNow.day.toString().padLeft(2,'0')}";
    String time = rightNow.toIso8601String().substring(11,19);
    return [date,time];
  }
}