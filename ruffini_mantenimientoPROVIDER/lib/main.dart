import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'Managers/issuesManager.dart';
import './Pages/mainPage.dart';


void main(){
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => IssuesManager()),
      ],
      child: MaterialApp(
    home: MainPage(),
    ),
    )
  );
}
