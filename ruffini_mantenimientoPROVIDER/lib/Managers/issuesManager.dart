import '../Models/issueModel.dart';
import '../Services/firebaseService.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class IssuesManager with ChangeNotifier {
  List<IssueModel> _notifiedIssuesList;
  List<IssueModel> _attendingIssuesList;

  IssuesManager() {
    _notifiedIssuesList = [];
    _attendingIssuesList = [];
    _listenToChangesOnNotifiedIssues();
    _listenToChangesOnAttendingIssues();
  }

  List<IssueModel> get notifiedIssuesList => _notifiedIssuesList;
  List<IssueModel> get attendingIssueslist => _attendingIssuesList;

  Future<void>_listenToChangesOnNotifiedIssues() async{
    FirebaseService().getNotifiedIssuesStream().listen(
      (newData) {
        newData.documentChanges.forEach(
          (change) => _manageChangeInNotifiedIssues(change)
        );
        notifyListeners();
      }
    );
  }

  Future<void> _listenToChangesOnAttendingIssues() async{
    FirebaseService().getAttendingIssuesStream().listen(
      (newData) {
        newData.documentChanges.forEach(
          (change) => _manageChangeInAttendingIssues(change)
        );
        notifyListeners();
      }
    );
  }

  _manageChangeInNotifiedIssues(DocumentChange change) {
    debugPrint("${change.type}");
    switch (change.type) {
      case DocumentChangeType.added:
        _notifiedIssuesList.add(IssueModel.fromDocumentSnapshot(change.document));
        debugPrint("Added issue with ID => ${change.document.documentID}");
        break;
      case DocumentChangeType.modified:
        //No modifications for now!
        break;
      case DocumentChangeType.removed:
        _notifiedIssuesList.removeWhere((issueModel)=>issueModel.id==change.document.documentID);
        debugPrint("Deleted issue with ID => ${change.document.documentID}");
        break;
    }
  }

  _manageChangeInAttendingIssues(DocumentChange change) {
    debugPrint("${change.type}");
    switch (change.type) {
      case DocumentChangeType.added:
        _attendingIssuesList.add(IssueModel.fromDocumentSnapshot(change.document));
        debugPrint("Added issue with ID => ${change.document.documentID}");
        break;
      case DocumentChangeType.modified:
        //No modifications for now!
        break;
      case DocumentChangeType.removed:
        _attendingIssuesList.removeWhere((issueModel)=>issueModel.id==change.document.documentID);
        debugPrint("Deleted issue with ID => ${change.document.documentID}");
        break;
    }
  }
}
