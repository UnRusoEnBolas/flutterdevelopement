import 'package:flutter/material.dart';
import 'package:ruffini_mantenimiento/Pages/issuesFinishedPage.dart';
import 'package:ruffini_mantenimiento/Pages/issuesInProcessPage.dart';
import '../Pages/mainPage.dart';
import '../Models/loginClass.dart';
import '../Models/firebaseClass.dart';
import './drawerBadge.dart';


class MyDrawer extends StatelessWidget {
  MyDrawer({@required Login user}) {
    _user = user;
  }

  Login _user;
  FirebaseAPI firebaseAPI = FirebaseAPI();

  @override
  Widget build(BuildContext context) {
    // Widget myDrawer(User currentUser, BuildContext context){
    // DatasheetManager datasheetManager = Provider.of<DatasheetManager>(context);
    // IssueManager issueManager = Provider.of<IssueManager>(context);
    // String numberNotFinishedDatasheets = datasheetManager.nNotFinished().toString();
    // String numberNotFinishedIssues = issueManager.nNotFinished().toString();
    return Drawer(
      elevation: 0,
      child: Container(
        color: Colors.blue,
        child: Column(
          children: <Widget>[
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.white),
              accountEmail: Text(_user.userCategory,
                  style: TextStyle(color: Colors.blue)),
              accountName: Text(
                _user.userName,
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
            ),
            ListTile(
              title: Text(
                "Averías notificadas",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 20),
              ),
              subtitle: Text(
                "Se muestran las averías activas en éste momento",
                style: TextStyle(color: Colors.white),
              ),
              trailing: DrawerBadge(type: 'notified',),
              onTap: ()=> Navigator.of(context).push(MaterialPageRoute(builder: (context)=>MainPage(user: _user,))),
            ),
            ListTile(
              title: Text(
                "Averías en proceso",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 20),
              ),
              subtitle: Text(
                "Se muestran las averías que están siendo atendidas en estos momentos",
                style: TextStyle(color: Colors.white),
              ),
              trailing: DrawerBadge(type: 'attending'),
              onTap: ()=> Navigator.of(context).push(MaterialPageRoute(builder: (context)=>InProcessPage(user: _user,))),
            ),
            ListTile(
              title: Text(
                "Averías finalizadas",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 20),
              ),
              subtitle: Text(
                "Se muestran las averías que se han atendido y dado como finalizadas",
                style: TextStyle(color: Colors.white),
              ),
              trailing: Icon(
                Icons.notifications_off,
                color: Colors.white,
                size: 24,
              ),
              onTap: ()=> Navigator.of(context).push(MaterialPageRoute(builder: (context)=>FinishedPage(user: _user))),
            ),
          ],
        ),
      ),
    );
  }
}
