import 'package:flutter/material.dart';
import '../Models/issueClass.dart';
import '../Models/loginClass.dart';

class IssueListTileFinalizada extends StatelessWidget {
  IssueModel _issue;
  Login _user;

  IssueListTileFinalizada({@required IssueModel issue, @required Login user}) {
    _issue = issue;
    _user = user;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ExpansionTile(
        initiallyExpanded: false,
        title: Text(
          _issue.issueType,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        subtitle: Row(
          children: <Widget>[
            Text("Resuelta el día "),
            Text(
              _issue.finishDate,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(" a las "),
            Text(
              _issue.finishTime,
              style: TextStyle(fontWeight: FontWeight.bold),
            )
          ],
        ),
        children: <Widget>[
          Column(
            children: <Widget>[
              expansionChild(
                  context, "Detalle de la avería: ", _issue.issueSubType),
              Divider(),
              expansionChild(
                  context, "Se notificó por el usuario: ", _issue.reporterUser),
              Divider(),
              expansionChild(context, "Fue notificada el día: ", _issue.notificationDate),
              Divider(),
              expansionChild(context, "Fue notificada a las: ", _issue.notificationTime),
              Divider(),
              expansionChild(context, "Estado de la avería: ", _issue.state),
              Divider(),
              expansionChild(
                  context, "Se atendió por el usuario: ", _issue.attendantUser),
              Divider(),
              elapsedTime(_issue.notificationDate, _issue.notificationTime,
                  _issue.finishDate, _issue.finishTime),
            ],
          )
        ],
      ),
    );
  }

  Widget expansionChild(BuildContext context, String field, String value) {
    return RichText(
      text: TextSpan(
          text: field,
          style: TextStyle(color: Colors.grey),
          children: [
            TextSpan(
                text: value,
                style:
                    TextStyle(color: Colors.blue, fontWeight: FontWeight.bold))
          ]),
    );
  }

  Widget elapsedTime(String startDate, String startTime, String finishDate,
      String finishTime) {
    String startFormat = "$startDate $startTime";
    String finishFormat = "$finishDate $finishTime";
    DateTime start = DateTime.parse(startFormat);
    DateTime end = DateTime.parse(finishFormat);
    Duration timeElapsed = end.difference(start);
    return Chip(
      backgroundColor: Colors.blue,
      label: Text(timeElapsed.toString().substring(0, 2) +
          " horas " +
          timeElapsed.toString().substring(3, 5) +
          " minutos"),
      labelStyle: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
