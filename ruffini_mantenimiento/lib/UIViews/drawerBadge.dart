import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:ruffini_mantenimiento/Models/firebaseClass.dart';

class DrawerBadge extends StatelessWidget {
  FirebaseAPI firebaseAPI = FirebaseAPI();
  String _type;

  DrawerBadge({@required String type}) {
    _type = type;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _type == 'attending'
          ? firebaseAPI.getAttendingIssuesStream()
          : firebaseAPI.getNotifiedIssuesStream(),
      builder: (_, issuesSnapshot) {
        if (!issuesSnapshot.hasData) {
          return CircularProgressIndicator();
        } else if (issuesSnapshot.data.documents.length == 0) {
          return Badge(
            showBadge: false,
            child: Icon(
              _type == "attending"
                  ? Icons.notifications_paused
                  : Icons.notifications_active,
              color: Colors.white,
            ),
          );
        } else {
          return Badge(
            child: Icon(
              _type == "attending"
                  ? Icons.notifications_paused
                  : Icons.notifications_active,
              color: Colors.white,
            ),
            animationType: BadgeAnimationType.fade,
            badgeColor: Colors.red,
            badgeContent: Text(
              issuesSnapshot.data.documents.length.toString(),
              style: TextStyle(color: Colors.white),
            ),
          );
        }
      },
    );
  }
}
