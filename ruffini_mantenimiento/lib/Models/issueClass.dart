import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:ruffini_mantenimiento/Models/firebaseClass.dart';
import './loginClass.dart';

class IssueModel{
  String _notificationDate;
  String _notificationTime;
  String _issueType;
  String _issueSubType;
  bool _isCompletelyFinished;
  String _reporterUser;
  String _state;
  String _attendDate;
  String _attendTime;
  String _attendantUser;
  bool _checkedFinishedMaintenance;
  bool _checkedFinishedOvens;
  String _deliverydate;
  String _deliveryTime;
  String _finishDate;
  String _finishTime;
  String _id;

  FirebaseAPI firebaseAPI;

  String get id => _id;
  String get reporterUser => _reporterUser;
  String get notificationDate => _notificationDate;
  String get notificationTime => _notificationTime;
  String get issueType => _issueType;
  String get issueSubType => _issueSubType;
  String get state => _state;
  String get attendantUser => _attendantUser;
  String get attendDate => _attendDate;
  String get attendTime => _attendTime;
  String get finishDate => _finishDate;
  String get finishTime => _finishTime;

  IssueModel.fromDocumentSnapshot(DocumentSnapshot snapShot){
    _id = snapShot.documentID;
    _reporterUser = snapShot.data['reporterUser'];
    _notificationDate = snapShot.data['notificationDate'];
    _notificationTime = snapShot.data['notificationTime'];
    _issueType = snapShot.data['issueType'];
    _issueSubType = snapShot.data['issueSubType'];
    _state = snapShot.data['state'];
    _attendantUser = snapShot.data['attendantUser'];
    _attendDate = snapShot.data['attendDate'];
    _attendTime = snapShot.data['attendTime'];
    _finishDate = snapShot.data['finishDate'];
    _finishTime = snapShot.data['finishTime'];
    firebaseAPI = FirebaseAPI();
  }

  void assign({@required Login user}){
    firebaseAPI.setIssueAsAttending(_id, user.userName);
  }

  void finish({@required Login user}){
    firebaseAPI.setIssueAsFinished(_id, user.userName);
  }

}