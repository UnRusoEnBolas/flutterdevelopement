import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseAPI{
  Stream<QuerySnapshot> getNotifiedIssuesStream() {
    return Firestore.instance.collection("Issues").where('state', isEqualTo: 'Notificada').snapshots();
  }

  Stream<QuerySnapshot> getAttendingIssuesStream() {
    return Firestore.instance.collection("Issues").where('state', isEqualTo: 'Atendiendo').snapshots();
  }

  Stream<QuerySnapshot> getFinishedIssuesStream() {
    return Firestore.instance.collection("Issues").where('state', isEqualTo: 'Finalizada').snapshots();
  }

  void setIssueAsAttending(String issueID, String userName){

    List<String> actualTime = _getActualTime();

    Firestore.instance.collection('Issues').document(issueID).updateData(
      {
        "state" : "Atendiendo",
        "attendantUser" : userName,
        "attendDate" : actualTime[0],
        "attendTime" : actualTime[1]
      }
    );
  }

  void setIssueAsFinished(String issueID, String userName){

    List<String> actualTime = _getActualTime();
    
    Firestore.instance.collection('Issues').document(issueID).updateData(
      {
        "state" : "Finalizada",
        "finishDate" : actualTime[0],
        "finishTime" : actualTime[1]
      }
    );
  }

  List<String> _getActualTime(){
    DateTime rightNow = DateTime.now();
    String date = "${rightNow.year}-${rightNow.month.toString().padLeft(2,'0')}-${rightNow.day.toString().padLeft(2,'0')}";
    String time = rightNow.toIso8601String().substring(11,19);
    return [date,time];
  }
}