import 'package:cloud_firestore/cloud_firestore.dart';

class Login {
  String _userName;
  String _userCode;
  String _userCategory;

  get userName => _userName;
  get userCode => _userCode;
  get userCategory => _userCategory;

  set userName(String userName) => _userName = userName;
  set userCode(String userCode) => _userCode = userCode;
  set userCategory(String userCategory) => _userCategory = userCategory;

  Login(this._userName, this._userCode);

  Login.fromSnapshot(DocumentSnapshot s) {
    _userName = s.data['name'];
    _userCode = s.data['code'];
    _userCategory = s.data['category'];
  }

  toJson() {
    return {"name": _userName, "code": _userCode, "category": _userCategory};
  }

  Future<bool> doesExist() async {
    bool isValid;
    QuerySnapshot snapshot = await Firestore.instance
        .collection('Users')
        .where('name', isEqualTo: _userName)
        .getDocuments();

    if (snapshot.documents.length == 0 ||
        snapshot.documents.first.data['code'] != _userCode) {
      isValid = false;
    } else {
      isValid = true;
      _userName = snapshot.documents.first.data['name'];
      _userCode = snapshot.documents.first.data['code'];
      _userCategory = snapshot.documents.first.data['category'];
    }
    return isValid;
  }
}
