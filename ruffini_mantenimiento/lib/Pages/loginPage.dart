import 'package:flutter/material.dart';
import 'mainPage.dart';
import '../Models/loginClass.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isValid = true;
  bool paintInRedUser = false;
  bool paintInRedCode = false;
  TextEditingController nameController = TextEditingController();
  TextEditingController codeController = TextEditingController();
  void validateUser() async {
    Login auxUser = Login(nameController.text, codeController.text);
    bool auxBool = await auxUser.doesExist();
    setState(() {
      FocusScope.of(context).unfocus();
      if (auxBool) {
        isValid = true;
        paintInRedUser = false;
        paintInRedCode = false;
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> MainPage(user: auxUser)));
      } else {
        isValid = false;
        paintInRedUser = true;
        paintInRedCode = true;
      }
    });
  }

  void clearFields() {
    setState(() {
      FocusScope.of(context).unfocus();
      isValid = true;
      nameController.clear();
      codeController.clear();
      paintInRedUser = false;
      paintInRedCode = false;
    });
  }

  void maybeChangeColorCode() {
    if (paintInRedCode == true) {
      setState(() {
        paintInRedCode = false;
      });
    }
  }

  void maybeChangeColorUser() {
    if (paintInRedUser == true) {
      setState(() {
        paintInRedUser = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
          child: Card(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.90,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top:10, bottom: 10),
                child: Text("Inicio de sesión",
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: Colors.blue,
                  fontSize: 35,
                  fontWeight: FontWeight.bold,
                ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                      labelText: "Nombre de Usuario",
                      prefixIcon: paintInRedUser
                          ? Icon(Icons.person,
                              size: 20, color: Colors.redAccent)
                          : Icon(Icons.person, size: 20),
                      enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: paintInRedUser
                                  ? Colors.red
                                  : Color(0xFF000000)))),
                  onChanged: (val) => maybeChangeColorUser(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
                child: TextField(
                  obscureText: true,
                  controller: codeController,
                  decoration: InputDecoration(
                      labelText: "Código de trabajador",
                      prefixIcon: paintInRedCode
                          ? Icon(Icons.vpn_key,
                              size: 20, color: Colors.redAccent)
                          : Icon(Icons.vpn_key, size: 20),
                      enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: paintInRedCode
                                  ? Colors.red
                                  : Color(0xFF000000)))),
                  onChanged: (val) => maybeChangeColorCode(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 30, 15, 0),
                child: Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  child: Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          color: Colors.blue.withAlpha(150),
                          blurRadius: 15.0,
                          spreadRadius: 0.5,
                          offset: Offset(3, 7))
                    ]),
                    child: RaisedButton(
                      color: Colors.blue,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Inicar Sesión",
                              style: TextStyle(color: Colors.white)),
                          Icon(
                            Icons.check,
                            color: Colors.white,
                          )
                        ],
                      ),
                      onPressed: () => validateUser(),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 15, 15, 30),
                child: Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  child: Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          color: Colors.red.withAlpha(150),
                          blurRadius: 15.0,
                          spreadRadius: 0.5,
                          offset: Offset(3, 7))
                    ]),
                    child: RaisedButton(
                      color: Colors.red,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text("Limpiar campos",
                              style: TextStyle(color: Colors.white)),
                          Icon(
                            Icons.clear,
                            color: Colors.white,
                          )
                        ],
                      ),
                      onPressed: () => clearFields(),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      )),
    );
  }
}