import 'package:flutter/material.dart';
import 'package:ruffini_mantenimiento/Models/loginClass.dart';
import '../Models/firebaseClass.dart';
import '../Models/issueClass.dart';
import '../UIViews/issueListTileFinalizada.dart';
import '../UIViews/drawer.dart';

class FinishedPage extends StatelessWidget {
  FirebaseAPI firebaseAPI = FirebaseAPI();
  Login user;

  FinishedPage({Login user}) {
    this.user = user;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          centerTitle: true,
          title: Text("Averías finalizadas"),
        ),
        drawer: MyDrawer(user: user,),
        body: StreamBuilder(
          stream: firebaseAPI.getFinishedIssuesStream(),
          builder: (_, issuesSnapshot) {
            if (!issuesSnapshot.hasData) {
              return CircularProgressIndicator();
            } else {
              return ListView.builder(
                itemCount: issuesSnapshot.data.documents.length,
                itemBuilder: (_, idx) {
                  IssueModel issue = IssueModel.fromDocumentSnapshot(
                      issuesSnapshot.data.documents.reversed.toList()[idx]);
                  return IssueListTileFinalizada(issue: issue, user: user);
                },
              );
            }
          },
        ));
  }
}