import 'package:flutter/material.dart';
import 'package:ruffini_mantenimiento/Models/loginClass.dart';
import '../Models/firebaseClass.dart';
import '../Models/issueClass.dart';
import '../UIViews/issueListTileNotificada.dart';
import '../UIViews/drawer.dart';

class MainPage extends StatelessWidget {
  FirebaseAPI firebaseAPI = FirebaseAPI();
  Login user;

  MainPage({Login user}) {
    this.user = user;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          centerTitle: true,
          title: Text("Averías notificadas"),
        ),
        drawer: MyDrawer(user: user,),
        body: StreamBuilder(
          stream: firebaseAPI.getNotifiedIssuesStream(),
          builder: (_, issuesSnapshot) {
            if (!issuesSnapshot.hasData) {
              return CircularProgressIndicator();
            } else {
              return ListView.builder(
                itemCount: issuesSnapshot.data.documents.length,
                itemBuilder: (_, idx) {
                  IssueModel issue = IssueModel.fromDocumentSnapshot(
                      issuesSnapshot.data.documents.reversed.toList()[idx]);
                  return IssueListTileNotificada(issue: issue, user: user);
                },
              );
            }
          },
        ));
  }
}
