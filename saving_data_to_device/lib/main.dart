import 'package:flutter/material.dart';
import 'ui/homepage.dart';

void main() {
  runApp( new MaterialApp(
    title: "My Anime List",
    home: MALHome(),
    debugShowCheckedModeBanner: false,
  ));
}