import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'dart:io';

class MALHome extends StatefulWidget {
  String data = "";
  @override
  _MALHomeState createState() => _MALHomeState();
}

class _MALHomeState extends State<MALHome> {
  TextEditingController _dataTextControl = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Anime list"),
        centerTitle: true,
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: TextField(
              controller: _dataTextControl,
              textInputAction: TextInputAction.send,

            ),
          ),
          RaisedButton(
            onPressed: () {
              writeData(_dataTextControl.text);
              _dataTextControl.text = '';
            },
            child: Text("Save this text"),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              RaisedButton(
                onPressed: () async {
                  String data = await readData();
                  setState(() {
                    widget.data=data;
                  });
                },
                child: Text("Read saved data"),
              ),
              RaisedButton(
                onPressed: () async {
                  clearData();
                  setState((){
                    widget.data = "";
                  });
                },
                child: Text("Clear saved data"),
              )
            ],
          ),
          Text(widget.data)
        ],
      ),
    );
  }

  Future<String> get _localPath async {
    final dir = await getApplicationDocumentsDirectory();
    return dir.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/data.txt');
  }

  Future<File> writeData(String data2write) async {
    final file = await _localFile;
    return file.writeAsString(data2write, mode: FileMode.writeOnlyAppend);
  }

  Future<String> readData() async {
    try {
      final file = await _localFile;
      return await file.readAsString();
    }
    catch(error) {
      return "¡Todavía no hay datos guardados!";
    }
  }

  void clearData() async {
    try {
      final file = await _localFile;
      file.writeAsString('');
    } catch (error) {return;}
  } 
}

