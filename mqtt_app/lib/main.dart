import 'package:flutter/material.dart';
import 'package:mqtt_client/mqtt_client.dart';

void _conectarBroker(MqttClient client, String username, String password) async {
  await client.connect(username, password);
  debugPrint("Conectado a broker MQTT!");
}

void main() async {
  runApp(
    MaterialApp(
      title: "MQTT",
      home: MQTTHome(),
      theme: ThemeData()
    )
  );
}

class MQTTHome extends StatefulWidget {
  @override
  _MQTTHomeState createState() => _MQTTHomeState();
}

class _MQTTHomeState extends State<MQTTHome> {
  MqttClient _clienteMQTT;
  Subscription _suscripcion;

  @override
  void initState() {
    super.initState();
    _clienteMQTT.onConnected();
    _clienteMQTT = MqttClient.withPort("io.adafruit.com", "androidEmulator", 1883);
    _conectarBroker(_clienteMQTT, "JGimenezP9", "d1d146900b1c47a598e398c233192420");
    _suscripcion = _clienteMQTT.subscribe("JGimenezP9/feeds/relay", MqttQos.atLeastOnce);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MQTT Client"),
        centerTitle: true,
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        )
      )
    );
  }

}