import 'package:flutter/material.dart';
import 'package:tts/tts.dart';

void main() => runApp(MaterialApp(
        home: Scaffold(
      body: Center(
        child: RaisedButton(
          onPressed: () => Tts.speak("Hello"),
          child: Text('Press to speak'),
        ),
      ),
    )));
